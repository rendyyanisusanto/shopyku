<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $profil_website['nama_website'] ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/dist')?>/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/dist')?>/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url('include/core/core.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('include/template/toastr/toastr.css')?>">
  <link rel="icon" href="<?php echo base_url('include/setting_website/'.$profil_website['icon'])?>">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
  <script src="<?php echo base_url('include/template/adminlte/bower_components')?>/jquery/dist/jquery.min.js"></script>
  
<script src="<?php echo base_url('include/core/core.js')?>"></script>
  
</head>