
<script type="text/javascript">
    $( "#app-submit" ).submit(function( e ) {
            e.preventDefault();
            $.ajax({
                url : $(this).attr('action') || window.location.pathname,
                type: "POST",
                data: $(this).serialize(),
                success: function (data) {
                    $('.se-pre-con').css('display','block');
                    $(".se-pre-con").fadeOut("slow");

                    toastr.success('Data berhasil diubah, Refresh untuk melihat perubahan');
                    $(".app-content").html(result);  
                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
     }); 


    $('#app-photo-submit').submit(function(e){
            e.preventDefault(); 
                 $.ajax({
                     url:$(this).attr('action') || window.location.pathname,
                     type:"post",
                     data:new FormData(this),
                     processData:false,
                     contentType:false,
                     cache:false,
                     async:false,
                      success: function(data){
                           $('.se-pre-con').css('display','block');
                            $(".se-pre-con").fadeOut("slow");

                            toastr.success('Foto Profil berhasil diubah, Refresh untuk melihat perubahan');
                            $(".app-content").html(result);
                   }
                 });
    });
</script>