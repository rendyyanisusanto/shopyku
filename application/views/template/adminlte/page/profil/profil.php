    <section class="content-header">
      <h1>
        Profil
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Profil</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->

      <div class="row">
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Foto</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="welcome/save_photo" id="app-photo-submit" method="POST">
                <center><img src="<?php echo base_url('include/user_account/'.$data_get['account']['foto'])?>" class="img-responsive img-rounded"></center>
                <br>
                <input type="file" name="foto" class="form-control">
                <br>
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Ganti Foto</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Profil</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="global_controller/save_profil" id="app-submit" method="POST">
                <input type="hidden" value="<?php echo $data_get['account']['id'] ?>" name="id">
                <label>Username</label>
                <input type="text" class="form-control" name="username" value="<?php echo $data_get['account']['username'] ?>">
                <br>
                <label>Password</label>
                <input type="password" class="form-control" name="password" >
                <br>
                <label>Email</label>
                <input type="email" class="form-control" name="email" value="<?php echo $data_get['account']['email'] ?>">
                <br>
                <label>First Name</label>
                <input type="text" class="form-control" name="first_name" value="<?php echo $data_get['account']['first_name'] ?>">
                <br>
                <label>Last Name</label>
                <input type="text" class="form-control" name="last_name" value="<?php echo $data_get['account']['last_name'] ?>">
                <br>
                <label>Phone</label>
                <input type="text" class="form-control" name="phone" value="<?php echo $data_get['account']['phone'] ?>">
                <br>
                <button type="submit"  class="btn btn-success btn-primary"><i class="fa fa-save"></i> Submit</button>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
