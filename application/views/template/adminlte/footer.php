<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Myber</a>.</strong> All rights
    reserved.
  </footer>
  <!-- Control Sidebar -->
</div>

<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/adminlte.min.js"></script>
<!--  -->
<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/demo.js"></script>

<script src="<?php echo base_url('include/template/toastr/toastr.min.js')?>"></script>

</body>
</html>