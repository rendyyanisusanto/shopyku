<ul class="sidebar-menu" data-widget="tree">
  <li class="header">MAIN NAVIGATION</li>
    <li class="active treeview menu-open">
      <a href="" class="app-item">
        <i class="fa fa-dashboard"></i> <span>Dashboard</span>
      </a>
  </li>
  <li class="treeview">
          <a href="#">
            <i class="fa fa-cubes"></i>
            <span>Tokoku</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="Barang/get_data" class="app-item"><i class="fa fa-circle-o"></i> Tambah Produk</a></li>
            <li><a href="Kategori_barang/get_data" class="app-item"><i class="fa fa-circle-o"></i> Kategori Produk</a></li>
            <li><a href="Testimoni/get_data" class="app-item"><i class="fa fa-circle-o"></i> Testimoni</a></li>
            <li><a href="service/get_data" class="app-item"><i class="fa fa-circle-o"></i> Our Service</a></li>
            <li><a href="special_event/get_data" class="app-item"><i class="fa fa-circle-o"></i> Special Event</a></li>
          </ul>
  </li>

  <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text-o"></i>
            <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="Transaksi/get_data" class="app-item"><i class="fa fa-circle-o"></i> Data Transaksi</a></li>
          </ul>
  </li>

  <li>
    <a href="subscriber/get_data" class="app-item">
      <i class="fa fa-user"></i> <span>Subscriber</span>
    </a>
  </li>

  <li>
    <a href="pesan/get_data" class="app-item">
      <i class="fa fa-envelope"></i> <span>Pesan</span>
    </a>
  </li>

   <li class="treeview">
          <a href="#">
            <i class="fa fa-cog"></i>
            <span>Setting</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="Profil_website/get_data" class="app-item"><i class="fa fa-circle-o"></i> Profil Website</a></li>
            <li><a href="About_us/get_data" class="app-item"><i class="fa fa-circle-o"></i> About Us</a></li>
            <li><a href="pengaturan_lain/get_data" class="app-item"><i class="fa fa-circle-o"></i> Pengaturan Lain</a></li>
          </ul>
        </li>
          
</ul>