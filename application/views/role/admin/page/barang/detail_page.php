 <section class="content">

      <!-- Small boxes (Stat box) -->
      
        <?php $i=0; ?>
      <?php foreach ($data_get['data_edit'] as $key => $value): ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Detail <?php echo $value['nama_barang'] ?></h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
             
                <div class="col-md-12">
                  <table class="table table-responsive table-bordered">
                    <tr>
                      <td width="20%">Nama Barang</td>
                      <td width="1%">:</td>
                      <td><?php echo $value['nama_barang'] ?></td>
                    </tr>

                    <tr>
                      <td width="20%">Harga Barang</td>
                      <td width="1%">:</td>
                      <td>Rp. <?php echo number_format($value['harga_barang'],0,',','.') ?></td>
                    </tr>

                    <tr>
                      <td width="20%">Stok</td>
                      <td width="1%">:</td>
                      <td><?php echo $value['stok'] ?></td>
                    </tr>

                    <tr>
                      <td width="20%">Diskon</td>
                      <td width="1%">:</td>
                      <td><?php echo ($value['diskon_type']=="uang")?'Rp. '.number_format($value['diskon'],0,',','.'):$value['diskon']; ?></td>
                    </tr>

                    <tr>
                      <td width="20%">Diskon Type</td>
                      <td width="1%">:</td>
                      <td><?php echo $value['diskon_type'] ?></td>
                    </tr>

                    <tr>
                      <td width="20%">Foto</td>
                      <td width="1%">:</td>
                      <td><img class="img-responsive" style="width: 100%;max-width: 350px;" src="<?php echo base_url('include/foto_produk/'.$value['foto']);?>"></td>
                    </tr>

                    <tr>
                      <td width="20%">Deskripsi</td>
                      <td width="1%">:</td>
                      <td><?php echo $value['deskripsi'] ?></td>
                    </tr>
                  </table>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
      <?php $i++; ?>
      <?php endforeach ?>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><center><a href="Barang/get_data" class="app-item btn btn-warning pull-center"><i class="fa fa-arrow-left"></i> Kembali</a></center></h3>
            </div>
           
          </div>
        </div>
      </div>
    </section>