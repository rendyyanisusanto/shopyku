
<script type="text/javascript">
    CKEDITOR.replace('deskripsi');
    $( "#app-submit" ).on('submit',function( e ) {
        for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

              var foto= $('#foto').prop('files')[0];

              var form_data = new FormData();
              form_data.append('nama_barang', $('#nama_barang').val());
              form_data.append('harga_barang', $('#harga_barang').val());
              form_data.append('deskripsi', $('#deskripsi').val());
              form_data.append('idkategori_fk', $('#idkategori_fk').val());
              form_data.append('stok', $('#stok').val());
              form_data.append('foto_produk', foto);

            e.preventDefault();

            $('.se-pre-con').css('display','block');
            send_ajax_file( $(this).attr('action'),form_data ).then( function(data){
                $(".se-pre-con").fadeOut("slow");
                toastr.success('Data berhasil ditambahkan, Refresh untuk melihat perubahan');
                set_content('barang/get_data');
            });
            
            return false;
     }); 
    
</script>
