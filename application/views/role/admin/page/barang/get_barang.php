<?php foreach ($data_get['barang'] as $key => $value): ?>
  <div class="col-md-4">
          <div class="box box-success box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><?php echo $value['nama_barang'] ?></h3>

              <div class="box-tools pull-right">
                <input type="checkbox" class="checkbox" name="get-check" value="<?php echo $value['id_barang'] ?>"></input>
              </div>
              <!-- /.box-tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <img class="img-responsive" style="width: 100%;" src="<?php echo base_url('include/foto_produk/'.$value['foto']);?>">
            </div>
            <!-- /.box-body -->
          </div>
        </div>
<?php endforeach ?>


        