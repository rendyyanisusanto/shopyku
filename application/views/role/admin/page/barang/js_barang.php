
<script type="text/javascript">
    set_produk();
    $('#filter_submit').on('click',function(){
        set_produk();
    });
    $('#reset_filter').on('click',function(){
        $("#nama_barang_search").val("");
        $("#with_search").val("");
        $("#stok_search").val("");
        $("#with_harga_search").val("");
        $("#harga_search").val("");
        set_produk();
    });  
    function set_produk(halaman=1,start=0,end=0,limit=6,nama_barang_search="", with_search="",stok_search="", with_harga_search="", harga_search="")
    {
        send_ajax( "barang/get_produk",{
                page:halaman,
                start:start,
                end:end,
                limit:limit,
                nama_barang_search: $("#nama_barang_search").val(),
                with_search         : $("#with_search").val(),
                stok_search         : $("#stok_search").val(),
                with_harga_search   : $("#with_harga_search").val(),
                harga_search        : $("#harga_search").val(),
            } ).then( function(data){

            set_pagination(halaman,
                start,
                end,
                limit,
                $("#nama_barang_search").val(),
                $("#with_search").val(),
                $("#stok_search").val()
                );
            $('.tbproduk').html(data);
            
        });
    }
    function set_pagination(halaman=1,start=0,end=0,limit=6,nama_barang_search="", with_search="",stok_search="", with_harga_search="", harga_search=""){
        send_ajax( "barang/get_pagination",{
                page:halaman,
                start:start,
                end:end,
                limit:limit,
                nama_barang_search:nama_barang_search,
                with_search:with_search,
                stok_search:stok_search,
                harga_search:harga_search,
                with_harga_search:with_harga_search
            } ).then( function(data){
            $('.pg').html(data);
        });
    }
    $("#del-btn").click(function(){
            var check = [];
            if ($("input[name='get-check']:checked").length==0) {

                toastr.options.positionClass = "toast-bottom-right";
                toastr.options.progressBar = true;
                toastr.options.closeButton = true;
                toastr.error('PILIH DATA TERLEBIH DAHULU');
            }else{
                $.each($("input[name='get-check']:checked"), function(){            
                    check.push($(this).val());
                });
                $.ajax({
                    url : 'barang/hapus',
                    type: "POST",
                    data: {data_get:check},
                    success: function (data) {
                        
                        $('.se-pre-con').css('display','block');
                        $(".se-pre-con").fadeOut("slow");
                        toastr.options.positionClass = "toast-bottom-right";
                        toastr.options.progressBar = true;
                        toastr.options.closeButton = true;
                        toastr.success('Data berhasil dihapus, Refresh untuk melihat perubahan');
                        set_content('barang/get_data');

                    },
                    error: function (jXHR, textStatus, errorThrown) {
                        alert(errorThrown);
                    }
                });
               
            }
    });

    $("#edit-btn").click(function(){
            var check = [];
            if ($("input[name='get-check']:checked").length==0) {

                toastr.options.positionClass = "toast-bottom-right";
                toastr.options.progressBar = true;
                toastr.options.closeButton = true;
                toastr.error('PILIH DATA TERLEBIH DAHULU');
            }else if ($("input[name='get-check']:checked").length>1) {

                toastr.options.positionClass = "toast-bottom-right";
                toastr.options.progressBar = true;
                toastr.options.closeButton = true;
                toastr.error('PILIH SATU DATA');
            }else{
                $.each($("input[name='get-check']:checked"), function(){            
                    check.push($(this).val());
                });
                set_content('barang/edit_page',{send_data:check});
            }
    });
    
    $("#detail-btn").click(function(){
            var check = [];
            if ($("input[name='get-check']:checked").length==0) {
                toastr.options.positionClass = "toast-bottom-right";
                toastr.options.progressBar = true;
                toastr.options.closeButton = true;
                toastr.error('PILIH DATA TERLEBIH DAHULU');
            }else{
                $.each($("input[name='get-check']:checked"), function(){            
                    check.push($(this).val());
                });
                set_content('barang/detail_page',{send_data:check});
            }
    });

    $("#cetak-btn").click(function(){
            var check = [];
            
                $.each($("input[name='get-check']:checked"), function(){            
                    check.push($(this).val());
                });
                set_content('barang/cetak_page',{send_data:check});
            
    });
    

</script>