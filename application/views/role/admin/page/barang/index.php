	
  	<link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net-bs/css/dataTables.bootstrap.min.css">

	<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

	<section class="content-header">
      <h1>
        Barang
        <small>Tokoku</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
          <div class="box box-solid">
            
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <a href="barang/add_page" class="app-item btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                  <a class="btn btn-success" id="edit-btn"><i class="fa fa-edit"></i> Edit</a>
                  <a class="btn btn-danger" id="del-btn"><i class="fa fa-close"></i> Hapus</a>
                  <a class="btn btn-info" id="cetak-btn"><i class="fa fa-print"></i> Cetak</a>
                  <a class="btn btn-primary" id="detail-btn"><i class="fa fa-eye"></i> Detail</a>

                </div>
                <div class="col-md-6">
                  <a class="btn btn-warning pull-right" align="right" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                  <i class="fa fa-filter"></i> Filter
                  </a>
                  <br><br>
                     
                      <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel panel-primary">
                          <div class="panel-body">
                            <label>Nama Barang</label>
                            <input type="text" class="form-control" name="" id="nama_barang_search">
                            <br>
                            
                            <br>
                            <label>Stok</label>
                            <table class="table">
                              <tr>
                                <td>
                                  <select class="form-control" id="with_search">
                                    <option value=">"> > </option>
                                    <option value="<"> < </option>
                                    <option value="="> = </option>
                                  </select>
                                </td>
                                <td>
                                  <input type="number" class="form-control" name="" id="stok_search">
                                </td>
                              </tr>
                            </table>
                            <label>Harga</label>
                            <table class="table">
                              <tr>
                                <td>
                                  <select class="form-control" id="with_harga_search">
                                    <option value=">"> > </option>
                                    <option value="<"> < </option>
                                    <option value="="> = </option>
                                  </select>
                                </td>
                                <td>
                                  <input type="number" class="form-control" name="" id="harga_search">
                                </td>
                              </tr>
                            </table>
                            <button class="btn btn-primary" id="filter_submit"><i class="fa fa-search"></i> Cari</button>
                            <button class="btn btn-warning" id="reset_filter"><i class="fa fa-refresh"></i> Reset</button>
                          </div>
                        </div>
                      </div>

                </div>
              </div>
              
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
      <div class="row">
        <div class="tbproduk"></div>
      </div>
      <div class="row">
        <div> 
          <center>
              <ul class="pagination pagination-sm no-margin pg">
                
              </ul>
          </center>
        </div>
      </div>
    </section>
