    
<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>
    <section class="content-header">
      <h1>
        Barang
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Barang</a></li>
        <li class="active">Tambah Barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
       <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="barang/simpan_data" id="app-submit" method="POST">
                <div class="col-md-12">
                  
                  <label>Nama Barang</label>
                  <input type="text" required="" class="form-control" name="nama_barang" id="nama_barang">
                  <br>

                  <label>Kategori</label>
                  <select class="form-control" name="idkategori_fk" id="idkategori_fk">
                    <?php foreach ($data_get['kategori_barang'] as $key => $value): ?>
                      <option value="<?php echo $value['id_kategori_barang'] ?>"><?php echo $value['kategori'] ?></option>
                    <?php endforeach ?>
                  </select>
                  <br>
                  <label>Harga Barang</label>
                  <input type="number" required="" class="form-control" name="harga_barang" id="harga_barang">
                  <br>

                  <label>Diskon</label>
                  <table style="width: 70%;" class="table table-bordered">
                    <tr>
                      <td class="bg-warning" style="width: 15%;">
                        <div class="input-group">
                          <input class="form-control" type="number" name="">
                          <span class="input-group-addon">%</span>
                        </div>
                          
                      </td>
                      <td class="bg-warning" style="width: 5%;"> atau </td>
                      <td class="bg-warning" style="width: 20%;">
                        <div class="input-group">
                          <span class="input-group-addon">Rp. </span>
                          <input class="form-control" type="number" name="">
                          
                        </div>
                      </td>
                      <td class="bg-warning" style="width: 5%;">=</td>
                      <td class="bg-warning" style="width: 20%;">
                         <div class="input-group">
                          <span class="input-group-addon">Rp. </span>
                          <input class="form-control" type="number" name="">
                          
                        </div>
                      </td>
                    </tr>
                  </table>
                  
                  <br>

                  <label>Stok</label>
                  <input type="number" required="" class="form-control" name="stok" id="stok">
                  <br>

                  <label>Foto Barang</label>
                  <input type="file" required="" class="form-control" id="foto" name="foto">
                  <br>

                  <label>Deskripsi</label>
                  <textarea id="deskripsi" required="" name="deskripsi"></textarea>
                  <br>
                  
                </div>
                
                <div class="col-md-12"><br><button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
    </section>