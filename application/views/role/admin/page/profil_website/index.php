	
  	<link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net-bs/css/dataTables.bootstrap.min.css">

	<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

	<section class="content-header">
      <h1>
        profil website
        <small>Tokoku</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">profil website</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">profil website</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form class="form-horizontal" action="profil_website/update_data" id="app-submit">
              <div class="row">
                  <div class="col-md-6">
                      <div class="form-group">
                        <label class="control-label col-sm-4" for="nama_website">Nama Website/Toko:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" value="<?php echo $data_get['profil']['nama_website'] ?>" id="nama_website" placeholder="Enter Website Name">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="singkatan">Singkatan Website/Toko:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" value="<?php echo $data_get['profil']['singkatan'] ?>" id="singkatan" placeholder="Enter max 5 digit (ex : MSD, MCD)">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="pemilik">Pemilik/founder/CEO:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" value="<?php echo $data_get['profil']['pemilik'] ?>" id="pemilik" placeholder="Enter founder name">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="alamat">Alamat:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" value="<?php echo $data_get['profil']['alamat'] ?>" id="alamat" placeholder="Enter address">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="no_hp">No. HP:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" value="<?php echo $data_get['profil']['no_hp'] ?>" id="no_hp" placeholder="Enter Phone Number">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="email">Email:</label>
                        <div class="col-sm-8">
                          <input type="email" class="form-control" id="email" value="<?php echo $data_get['profil']['email'] ?>" placeholder="Enter email">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="facebook">Facebook:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="facebook" value="<?php echo $data_get['profil']['facebook'] ?>" placeholder="Enter facebook url">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="twitter">Twitter:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="twitter" value="<?php echo $data_get['profil']['twitter'] ?>" placeholder="Enter twitter url">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="twitter">Instagram:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="instagram" value="<?php echo $data_get['profil']['instagram'] ?>" placeholder="Enter instagram url">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-4" for="youtube">Youtube:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" id="youtube" value="<?php echo $data_get['profil']['youtube'] ?>" placeholder="Enter youtube">
                        </div>
                      </div>

                  </div>
                  <div class="col-md-6">
                    <label>Logo</label>
                    <br>
                    <center><img class="img img-responsive" src="<?php echo base_url('include/setting_website/logo/'.$data_get['profil']['logo']) ?>"></center>
                    <br>
                    <input type="file" name="logo" id="logo" class="form-control">
                  </div>
               </div>
               <hr>
               <div class="row">
                 <div class="col-md-12">
                   <center><button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button></center>
                 </div>
               </div>
               </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
