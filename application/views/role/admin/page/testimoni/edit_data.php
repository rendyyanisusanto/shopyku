    <section class="content-header">
      <h1>
        Profil
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> testimoni</a></li>
        <li class="active">Edit testimoni</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Small boxes (Stat box) -->
      <form action="testimoni/update_data" id="app-submit" method="POST">
        <?php $i=0; ?>
      <?php foreach ($data_get['data_edit'] as $key => $value): ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Testimoni</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
              <input type="hidden" name="id_testimoni" id="id_testimoni" value="<?php echo $value['id_testimoni'] ?>">
                <div class="col-md-12">
                  
                  <label>Nama</label>
                  <input type="text" class="form-control" required="" name="nama" id="nama" value="<?php echo $value['nama'] ?>">
                  <br>

                  <label>Foto</label>
                  <img src="<?php echo base_url('include/testimoni/'.$value['foto']) ?>" class="img img-responsive" style="max-width: 300px;">
                  <input type="file" class="form-control" id="foto" name="foto">
                  <br>

                  <label>Testimoni</label>
                  <textarea id="testimoni" name="testimoni" required="" ><?php echo $value['testimoni'] ?></textarea>
                  <br>
                  
                </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
      <?php $i++; ?>
      <?php endforeach ?>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><center><button type="submit" class="btn btn-success pull-center"><i class="fa fa-save"></i> Simpan</button></center></h3>
            </div>
           
          </div>
        </div>
      </div>
       </form>
    </section>