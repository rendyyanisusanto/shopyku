
<script type="text/javascript">
    CKEDITOR.replace('testimoni');
    $( "#app-submit" ).on('submit',function( e ) {
        for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }

              var foto= $('#foto').prop('files')[0];

              var form_data = new FormData();
              form_data.append('nama', $('#nama').val());
              form_data.append('testimoni', $('#testimoni').val());
              form_data.append('foto', foto);
            e.preventDefault();
            $.ajax({
                url : $(this).attr('action') || window.location.pathname,
                type: "POST",
                data:form_data,
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function (data) {
                    
                    $('.se-pre-con').css('display','block');
                    $(".se-pre-con").fadeOut("slow");
                    
                    toastr.success('Data berhasil diubah, Refresh untuk melihat perubahan');
                    set_content('testimoni/get_data');

                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
     }); 
</script>
