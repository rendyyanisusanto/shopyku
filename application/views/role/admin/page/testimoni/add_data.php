    <section class="content-header">
      <h1>
        testimoni
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> testimoni</a></li>
        <li class="active">Tambah testimoni</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
       <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar testimoni</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="testimoni/simpan_data" id="app-submit" method="POST">
                <div class="col-md-12">
                  
                  <label>Nama</label>
                  <input type="text" class="form-control" required="" name="nama" id="nama">
                  <br>

                  <label>Foto</label>
                  <input type="file" class="form-control" required="" id="foto" name="foto">
                  <br>

                  <label>Testimoni</label>
                  <textarea id="testimoni" name="testimoni" required=""></textarea>
                  <br>
                  
                </div>
                
                <div class="col-md-12"><br><button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
    </section>