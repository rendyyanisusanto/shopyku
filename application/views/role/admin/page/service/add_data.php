    <section class="content-header">
      <h1>
        service
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> service</a></li>
        <li class="active">Tambah service</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
       <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar service</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="service/simpan_data" id="app-submit" method="POST">
                <div class="col-md-12">
                  
                  <label>judul</label>
                  <input type="text" class="form-control" name="judul" id="judul">
                  <br>

                  <label>text</label>
                  <input type="text" class="form-control" id="txt" name="txt">
                  <br>

                  <label>icon</label>
                  <select class="form-control" id="iconion" name="iconion">
                    <option value="pie_chart">pie_chart</option>
                    <option value="backspace">backspace</option>
                    <option value="av_timer">av_timer</option>
                    <option value="beenhere">business_center</option>
                    <option value="cloud_done">cloud_done</option>
                  </select>
                  <br>
                  
                </div>
                
                <div class="col-md-12"><br><button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
    </section>