 <script type="text/javascript">
    $("textarea").each(function(){
    CKEDITOR.replace( this );
    });
    $( "#app-submit" ).on('submit',function( e ) {
        for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
              var foto= $('#foto').prop('files')[0];

              var form_data = new FormData();
              form_data.append('nama_barang', $('.nama_barang').val());
              form_data.append('harga_barang', $('.harga_barang').val());
              form_data.append('deskripsi', $('.deskripsi').val());
              form_data.append('stok', $('.stok').val());
              form_data.append('foto_lama', $('.foto_lama').val());
              form_data.append('id_barang', $('.id_barang').val());
              form_data.append('foto_produk', foto);

            e.preventDefault();
            $.ajax({
                url : $(this).attr('action') || window.location.pathname,
                type: "POST",
                data:form_data,
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function (data) {
                    
                    $('.se-pre-con').css('display','block');
                    $(".se-pre-con").fadeOut("slow");
                    
                    toastr.success('Data berhasil diubah, Refresh untuk melihat perubahan');
                    set_content('barang/get_data');

                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
     }); 
</script>
