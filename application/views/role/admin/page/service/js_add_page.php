
<script type="text/javascript">
    $( "#app-submit" ).on('submit',function( e ) {
         e.preventDefault();
            $('.se-pre-con').css('display','block');
           alert();
            $.ajax({
                url : $(this).attr('action') || window.location.pathname,
                type: "POST",
                data:$(this).serialize(),
                success: function (data) {
                    $(".se-pre-con").fadeOut("slow");
                    set_content('service/get_data');
                    toastr.success('Data berhasil diubah, Refresh untuk melihat perubahan');
                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
     }); 
</script>
