	
  	<link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net-bs/css/dataTables.bootstrap.min.css">

	<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

	<section class="content-header">
      <h1>
        subscriber
        <small>Tokoku</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">subscriber</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">subscriber</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <a href="subscriber/add_page" class="app-item btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                  <a class="btn btn-success" id="edit-btn"><i class="fa fa-edit"></i> Edit</a>
                  <a class="btn btn-danger" id="del-btn"><i class="fa fa-close"></i> Hapus</a>

                </div>
                
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-bordered" width="100%" id="tabel-data">
                    <thead>
                      <tr>
                        <th width="1%"><input type="checkbox" id="select_all" name=""></th>
                        <th>Email</th>
                        <th>Create At</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
