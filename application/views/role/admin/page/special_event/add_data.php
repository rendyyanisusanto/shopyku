    <section class="content-header">
      <h1>
        special event
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> special event</a></li>
        <li class="active">Tambah special event</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
       <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar special event</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="special_event/simpan_data" id="app-submit" method="POST">
                <div class="col-md-12">
                  
                  <label>judul</label>
                  <input type="text" class="form-control" name="judul" id="judul">
                  <br>

                  <label>Deskripsi</label>
                  <textarea id="deskripsi" required="" name="deskripsi"></textarea>
                  <br>

                  <label>Akhir Event</label>
                  <input type="date" class="form-control" id="sampai_tanggal" name="sampai_tanggal">
                  <br>
                  
                </div>
                
                <div class="col-md-12"><br><button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
    </section>