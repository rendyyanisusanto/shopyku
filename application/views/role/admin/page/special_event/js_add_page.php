
<script type="text/javascript">
     CKEDITOR.replace('deskripsi');
    $( "#app-submit" ).on('submit',function( e ) {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            e.preventDefault();
            $.ajax({
                url : $(this).attr('action') || window.location.pathname,
                type: "POST",
                data:$(this).serialize(),
                success: function (data) {
                    
                    $('.se-pre-con').css('display','block');
                    $(".se-pre-con").fadeOut("slow");
                    
                    toastr.success('Data berhasil diubah, Refresh untuk melihat perubahan');
                    set_content('special_event/get_data');

                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
     }); 
</script>
