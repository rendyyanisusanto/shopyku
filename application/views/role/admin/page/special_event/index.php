	
  	<link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net-bs/css/dataTables.bootstrap.min.css">

	<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net/js/jquery.dataTables.min.js"></script>
	<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

	<section class="content-header">
      <h1>
        special event
        <small>Tokoku</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">special event</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">special event</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="row">
                <div class="col-md-6">
                  <a href="special_event/add_page" class="app-item btn btn-primary"><i class="fa fa-plus"></i> Tambah Data</a>
                  <a class="btn btn-success" id="edit-btn"><i class="fa fa-edit"></i> Edit</a>
                  <a class="btn btn-danger" id="del-btn"><i class="fa fa-close"></i> Hapus</a>
                  <a class="btn btn-info" id="cetak-btn"><i class="fa fa-print"></i> Cetak</a>

                </div>
                <div class="col-md-6">
                  <a class="btn btn-warning pull-right" align="right" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" class="collapsed">
                  <i class="fa fa-filter"></i> Filter
                  </a>
                  <br><br>
                     
                      <div id="collapseOne" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                        <div class="panel panel-primary">
                          <div class="panel-body">
                            <label>Tanggal Mulai</label>
                            <input type="date" class="form-control" name="" id="tanggal_mulai_search">

                            <label>Tanggal Selesai</label>
                            <input type="date" class="form-control" name="" id="tanggal_selesai_search">

                            <label>Nama Kegiatan</label>
                            <input type="text" class="form-control" name="" id="nama_special_event_search">
                            <br>
                            <button class="btn btn-primary" id="filter_submit"><i class="fa fa-search"></i> Cari</button>
                          </div>
                        </div>
                      </div>

                </div>
              </div>
              <hr>
              <div class="row">
                <div class="col-md-12">
                  <table class="table table-bordered" width="100%" id="tabel-data">
                    <thead>
                      <tr>
                        <th width="1%">#</th>
                        <th>Judul</th>
                        <th>Deskripsi</th>
                        <th>Akhir Event</th>
                      </tr>
                    </thead>
                  </table>
                </div>
              </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
