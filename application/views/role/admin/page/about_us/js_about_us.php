<script type="text/javascript">
      CKEDITOR.replace( 'about_us' );
    $( "#app-submit" ).on('submit',function( e ) {
            var form_data = new FormData();
            var foto= $('#img_about_us').prop('files')[0];
            form_data.append('tagline', $('#tagline').val());
            form_data.append('about_us', CKEDITOR.instances.about_us.getData());
            form_data.append('img_about_us', foto);
            
            e.preventDefault();
            $.ajax({
                url : $(this).attr('action') || window.location.pathname,
                type: "POST",
                data:form_data,
                processData:false,
                contentType:false,
                cache:false,
                async:false,
                success: function (data) {
                    
                    $('.se-pre-con').css('display','block');
                    $(".se-pre-con").fadeOut("slow");
                    
                    toastr.success('Data berhasil diubah, Refresh untuk melihat perubahan');
                    set_content('about_us/get_data');

                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
     });
</script>