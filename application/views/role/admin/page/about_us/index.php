<script type="text/javascript" src="<?php echo base_url('include/ckeditor/ckeditor.js'); ?>"></script>

	<section class="content-header">
      <h1>
        profil website
        <small>Tokoku</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">profil website</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
      	<div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">profil website</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form class="form-horizontal" action="about_us/update_data" id="app-submit">
              <div class="row">
                  <div class="col-md-12">

                      <div class="form-group">
                        <label class="control-label col-sm-2" for="tagline">Tagline:</label>
                        <div class="col-sm-8">
                          <input type="text" class="form-control" value="<?php echo $data_get['profil']['tagline'] ?>" id="tagline" placeholder="Enter Tagline">
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-2" for="about_us">About us:</label>
                        <div class="col-sm-8">
                          <textarea class="form-control" id="about_us"><?php echo $data_get['profil']['about_us'] ?></textarea>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-sm-2" for="about_us">Image About Us:</label>
                        <div class="col-sm-8">
                          <?php if (!empty($data_get['profil']['img_about_us'])): ?>
                            <center><img src="<?php echo base_url('include/setting_website/img_about_us/'.$data_get['profil']['img_about_us']) ?>" class="img img-responsive"></center>  
                          <?php endif ?>
                          <input type="file" class="form-control"  id="img_about_us" placeholder="Enter Tagline">
                        </div>
                      </div>
                  </div>
               </div>
               <hr>
               <div class="row">
                 <div class="col-md-12">
                   <center><button class="btn btn-success" type="submit"><i class="fa fa-save"></i> Simpan</button></center>
                 </div>
               </div>
               </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
      </div>
    </section>
