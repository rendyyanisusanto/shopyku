<script type="text/javascript">
	$("#app-submit").on("submit", function(e){
		var form_data = new FormData(this);
		e.preventDefault();

            $('.se-pre-con').css('display','block');
            send_ajax_file( $(this).attr('action'),form_data ).then( function(data){
                $(".se-pre-con").fadeOut("slow");
                toastr.success('Data berhasil ditambahkan, Refresh untuk melihat perubahan');
                set_content('Transaksi/get_data');
            });
            
            return false;
	});
	
</script>