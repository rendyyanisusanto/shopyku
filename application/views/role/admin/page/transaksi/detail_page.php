 
 <section class="content">

      <!-- Small boxes (Stat box) -->
      
       
      <div class="row">
        <div class="col-md-6">
          <div class="box box-solid">
            <div class="box-header bg-navy with-border">
              <h3 class="box-title">Detail Transaksi</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-12">
                  <table class="table table-responsive table-bordered">
                    <tr>
                      <td width="30%" class="bg-primary">Kode Transaksi</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['kode_transaksi'] ?></td>
                    </tr>

                    <tr>
                      <td width="30%" class="bg-primary">Nama</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['nama'] ?></td>
                    </tr>

                    <tr>
                      <td width="30%" class="bg-primary">No Hp</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['no_hp'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Email</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['email'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Provinsi</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['provinsi'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Kota</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['kota'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Alamat</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['alamat'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Kode Pos</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['kode_pos'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Ekspedisi</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['ekspedisi'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Paket Ekspedisi</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['paket_ekspedisi'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Metode Pembayaran</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo $data_get['transaksi']['metode_pembayaran'] ?></td>
                    </tr>
                    <tr>
                      <td width="30%" class="bg-primary">Status</td>
                      <td width="1%" class="bg-info">:</td>
                      <td class="bg-info"><?php echo ($data_get['transaksi']['status_transaksi']==0) ? "<span class='label label-danger'>Belum Dikonfirmasi</span>":"<span class='label label-success'>Terkonfirmasi</span>"?></td>
                    </tr>
                    <?php if ($data_get['transaksi']['status_transaksi']==1): ?>
                      <tr>
                        <td width="30%" class="bg-primary">Status</td>
                        <td width="1%" class="bg-info">:</td>
                        <td class="bg-info">
                          <img src="<?php echo base_url('include/foto_bukti/'.$data_get['transaksi']['foto_bukti'] ) ?>" style="max-width: 250px">
                        </td>
                      </tr>
                    <?php endif ?>
                  </table>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
       <div class="row">
         <div class="col-md-6">
           <div class="col-md-12">
              <div class="box box-solid">
                <div class="box-header bg-navy with-border">
                  <h3 class="box-title">Detail Barang</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                  <table class="table table-bordered" >
                         <tr>
                            <th></th>
                           <th>Produk</th>
                           <th>Berat</th>
                           <th>Harga</th>
                           <th width="20%">QTY</th>
                         </tr>
                       <?php foreach ($data_get['barang_transaksi'] as $key => $value): ?>
                         <tr >
                            <td><img src="<?php echo base_url('include/foto_produk/'.$value['foto']);?>" style="max-width: 100px;" alt="Image" class="img-fluid"></td>
                           <td style="vertical-align: middle;"><?php echo $value['nama_barang'] ?></td>
                           <td style="vertical-align: middle;"><?php echo $value['berat_barang'] ?> Gram</td>
                           <td style="vertical-align: middle;">Rp. <?php echo number_format($value['harga_barang'],0,',','.') ?></td>
                           <td style="vertical-align: middle;"><input type="number" disabled="" class="form-control qty" name="qty"value="<?php echo $value['qty'] ?>"></td>
                         </tr>
                       <?php endforeach ?>

                </table>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="box box-solid">
  
                <!-- /.box-header -->
                <div class="box-body">
                  <ul class="list-group">
                    <li class="list-group-item "><span class="pull-right">Rp. <?php echo number_format($data_get['transaksi']['total'],0,',','.') ?></span> Total Belanja</li>
                    <li class="list-group-item "><span class="pull-right">Rp. <?php echo number_format($data_get['transaksi']['ongkir'],0,',','.') ?></span> Ongkir</li>
                    <li class="list-group-item list-group-item-danger"> <b><span class="pull-right">Rp. <?php echo number_format(($data_get['transaksi']['total']+$data_get['transaksi']['ongkir']),0,',','.') ?></span> Total</b></li>             
                </ul>
                </div>
              </div>
            </div>
          </div>
          </div>

          
         </div>
       </div>
       

      
      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><center><a href="Transaksi/get_data" class="app-item btn btn-warning pull-center"><i class="fa fa-arrow-left"></i> Kembali</a></center></h3>
            </div>
           
          </div>
        </div>
      </div>
    </section>