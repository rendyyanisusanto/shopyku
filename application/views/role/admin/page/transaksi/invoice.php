 <section class="content">
<section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> <?php echo $profil_website['nama_website'] ?> 
            <small class="pull-right">Tanggal Transaksi: <?php echo date_format(date_create($data_get['transaksi']['create_at']),'d-m-Y') ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong><?php echo $profil_website['pemilik'] ?> .</strong><br>
            <?php echo $profil_website['alamat'] ?><br>
            <?php echo $data_get['city']['value_add'] ?>, <?php echo $data_get['province']['value_add'] ?> <?php echo $data_get['postal_code']['value_add'] ?><br>
            Phone: (+62)<?php echo $profil_website['no_hp'] ?><br>
            Email: <?php echo $profil_website['email'] ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php echo $data_get['transaksi']['nama'] ?></strong><br>
            <?php echo $data_get['transaksi']['alamat'] ?>
            <?php echo $data_get['transaksi']['kota'] ?>, <?php echo $data_get['transaksi']['provinsi'] ?> <?php echo $data_get['transaksi']['kode_pos'] ?><br>
            Phone: <?php echo $data_get['transaksi']['no_hp'] ?><br>
            Email: <?php echo $data_get['transaksi']['email'] ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #<?php echo $data_get['transaksi']['kode_transaksi'] ?></b><br>
          <br>
          <b>Order ID:</b> 4F3S8J<br>
          <b>Batas Pembayaran:</b> <?php echo date('d-m-Y', strtotime('+6 days', strtotime($data_get['transaksi']['create_at']))); ?><br>
          <b>Transfer Ke:</b> <?php echo $data_get['transaksi']['metode_pembayaran'] ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
                <th>Produk</th>
                <th>Berat</th>
                <th>Harga</th>
                <th width="20%">QTY</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($data_get['barang_transaksi'] as $key => $value): ?>
                <tr>
                  <td style="vertical-align: middle;"><?php echo $value['nama_barang'] ?></td>
                  <td style="vertical-align: middle;"><?php echo $value['berat_barang'] ?> Gram</td>
                  <td style="vertical-align: middle;">Rp. <?php echo number_format($value['harga_barang'],0,',','.') ?></td>
                  <td style="vertical-align: middle;"><?php echo $value['qty'] ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Metode Pembayaran:</p>
          
          <b><?php echo $data_get['transaksi']['metode_pembayaran'] ?></b>

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Kirimkan pembayaran ke Rekening <?php echo $data_get['jenis_pembayaran']['no_rek'] ?> atas nama <?php echo $data_get['jenis_pembayaran']['atas_nama'] ?>. Setelah selesai pembayaran mohon konfirmasi ke <b>No HP / WA (+62)<?php echo $profil_website['no_hp'] ?></b>.
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Batas Pembayaran <?php echo date('d-m-Y', strtotime('+6 days', strtotime($data_get['transaksi']['create_at']))); ?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>Rp. <?php echo number_format($data_get['transaksi']['total'],0,',','.') ?></td>
              </tr>
              
              <tr>
                <th>Ongkir:</th>
                <td>Rp. <?php echo number_format($data_get['transaksi']['ongkir'],0,',','.') ?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>Rp. <?php echo number_format(($data_get['transaksi']['total']+$data_get['transaksi']['ongkir']),0,',','.') ?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="invoice-print.html" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
    </section>
</section>