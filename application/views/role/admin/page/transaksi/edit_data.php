    <section class="content-header">
      <h1>
        Profil
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> barang</a></li>
        <li class="active">Edit barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Small boxes (Stat box) -->
      <form action="barang/update_data" id="app-submit" method="POST">
        <?php $i=0; ?>
      <?php foreach ($data_get['data_edit'] as $key => $value): ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar Jadwal kegiatan sekolah</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
              <input type="hidden" name="id_barang" class="id_barang" value="<?php echo $value['id_barang'] ?>">
                <div class="col-md-12">
                  
                  <label>Nama Barang</label>
                  <input type="text" class="form-control nama_barang" name="nama_barang" id="" value="<?php echo $value['nama_barang'] ?>" >
                  <br>

                  <label>Harga Barang</label>
                  <input type="number" class="form-control harga_barang" name="harga_barang" id="" value="<?php echo $value['harga_barang'] ?>">
                  <br>

                  <label>Stok</label>
                  <input type="number" class="form-control stok" name="stok" id="" value="<?php echo $value['stok'] ?>">
                  <br>

                  <label>Foto Barang</label>
                  <input type="" class="form-control foto_lama" id="" name="foto_lama" value="<?php echo $value['foto'] ?>">
                  <input type="file" class="form-control" id="foto" name="foto">
                  <br>

                  <label>Deskripsi</label>
                  <textarea class="deskripsi" name="deskripsi"><?php echo $value['deskripsi'] ?></textarea>
                  <br>
                </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
      <?php $i++; ?>
      <?php endforeach ?>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><center><button type="submit" class="btn btn-success pull-center"><i class="fa fa-save"></i> Simpan</button></center></h3>
            </div>
           
          </div>
        </div>
      </div>
       </form>
    </section>