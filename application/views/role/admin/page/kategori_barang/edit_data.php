    <section class="content-header">
      <h1>
        Profil
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> kategori_barang</a></li>
        <li class="active">Edit kategori_barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Small boxes (Stat box) -->
      <form action="kategori_barang/update_data" id="app-submit" method="POST">
        <?php $i=0; ?>
      <?php foreach ($data_get['data_edit'] as $key => $value): ?>
      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar kategori_barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
            
              <input type="hidden" name="id_kategori_barang" id="id_kategori_barang" value="<?php echo $value['id_kategori_barang'] ?>">
                <div class="col-md-12">
                  
                  <label>Nama</label>
                  <input type="text" class="form-control" required="" name="kategori" id="nama" value="<?php echo $value['kategori'] ?>">
                  <br>

                </div>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
      <?php $i++; ?>
      <?php endforeach ?>

      <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title"><center><button type="submit" class="btn btn-success pull-center"><i class="fa fa-save"></i> Simpan</button></center></h3>
            </div>
           
          </div>
        </div>
      </div>
       </form>
    </section>