    <section class="content-header">
      <h1>
        kategori_barang
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> kategori_barang</a></li>
        <li class="active">Tambah kategori_barang</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
       <div class="row">
        <div class="col-md-12">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Daftar kategori_barang</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <form action="kategori_barang/simpan_data" id="app-submit" method="POST">
                <div class="col-md-12">
                  
                  <label>Kategori</label>
                  <input type="text" class="form-control" required="" name="kategori" id="kategori">
                  <br>

                </div>
                
                <div class="col-md-12"><br><button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button></div>
              </form>
            </div>
            <!-- /.box-body -->
          </div>
        </div>
       
      </div>
    </section>