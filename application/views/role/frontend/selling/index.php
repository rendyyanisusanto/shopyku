<!DOCTYPE html>
<html lang="en">
  <head>
    <title><?php echo $profil_website['nama_website'] ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    
    
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/font-awesome/css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Muli:300,400,700,900" rel="stylesheet">
    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/fonts/');?>icomoon/style.css">

    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>jquery-ui.css">
    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>owl.theme.default.min.css">
    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>owl.theme.default.min.css">

    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>jquery.fancybox.min.css">

    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>bootstrap-datepicker.css">

    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/fonts/');?>flaticon/font/flaticon.css">

    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>aos.css">

    <link rel="stylesheet" href="<?php echo base_url('include/template/selling/css/');?>style.css">

    <link rel="stylesheet" href="<?php echo base_url('include/sweetalert2/dist/');?>sweetalert2.min.css">
    
  <link rel="stylesheet" href="<?php echo base_url('include/template/toastr/toastr.css')?>">
    <?php $this->load->view('role/frontend/selling/css_selling') ?>
  </head>
  <body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">
  
  <div class="site-wrap">

    <div class="site-mobile-menu site-navbar-target">
      <div class="site-mobile-menu-header">
        <div class="site-mobile-menu-close mt-3">
          <span class="icon-close2 js-menu-toggle"></span>
        </div>
      </div>
      <div class="site-mobile-menu-body"></div>
    </div>
   
    <div class="top-bar py-3 bg-light" id="home-section">
      <div class="container">
        <div class="row align-items-center">
         
          <div class="col-4 text-left">
            <ul class="social-media">
              <li><a target="__blank" href="<?php echo $profil_website['facebook'] ?>" class=""><span class="icon-facebook"></span></a></li>
              <li><a target="__blank" href="<?php echo $profil_website['twitter'] ?>" class=""><span class="icon-twitter"></span></a></li>
              <li><a target="__blank" href="<?php echo $profil_website['instagram'] ?>" class=""><span class="icon-instagram"></span></a></li>
              <li><a target="__blank" href="<?php echo $profil_website['youtube'] ?>" class=""><span class="icon-youtube"></span></a></li>
            </ul>
          </div>
          <div class="col-8">
            <p class="mb-0 float-right">
              <span class="mr-3"><a href="tel://+62<?php echo $profil_website['no_hp'] ?>"> <span class="icon-phone mr-2" style="position: relative; top: 2px;"></span><span class="d-none d-lg-inline-block text-black">+62<?php echo $profil_website['no_hp'] ?></span></a></span>
              <span><a href="mailto:<?php echo $profil_website['email'] ?>?Subject=Hello%20again"><span class="icon-envelope mr-2" style="position: relative; top: 2px;"></span><span class="d-none d-lg-inline-block text-black"><?php echo $profil_website['email'] ?></span></a></span>

              <span class="mr-3"><a href="<?php echo base_url('frontend/checkout') ?>"> <span class="icon-shopping-cart mr-2" style="position: relative; top: 2px;"></span><span class="d-none d-lg-inline-block text-black app-count-cart">Rp. <?php echo number_format($total,0,',','.')."(".$total_items." item(s))" ?></span></a></span>
            </p>
            
          </div>
        </div>
      </div> 
    </div>

    <header class="site-navbar py-4 bg-white js-sticky-header site-navbar-target" role="banner">

      <div class="container">
        <div class="row align-items-center">
          
          <div class="col-6 col-xl-2">
            <h1 class="mb-0 site-logo"><a href="index.html" class="text-black mb-0"><?php echo $profil_website['nama_website'] ?> </a></h1>
          </div>
          <div class="col-12 col-md-10 d-none d-xl-block">
            <nav class="site-navigation position-relative text-right" role="navigation">

              <ul class="site-menu main-menu js-clone-nav mr-auto d-none d-lg-block">
                <li><a href="#home-section" class="nav-link">Home</a></li>
                <li><a href="#products-section" class="nav-link">Products</a></li>
                <li><a href="#about-section" class="nav-link">About Us</a></li>
                <li><a href="#testimonials-section" class="nav-link">Testimonials</a></li>                
                <li><a href="#contact-section" class="nav-link">Contact</a></li>
              </ul>
            </nav>
          </div>


          <div class="col-6 d-inline-block d-xl-none ml-md-0 py-3" style="position: relative; top: 3px;"><a href="#" class="site-menu-toggle js-menu-toggle text-black float-right"><span class="icon-menu h3"></span></a></div>

        </div>
      </div>
      
    </header>

  
     
    <div class="site-blocks-cover overlay" style="background-image: url(<?php echo base_url('include/setting_website/background/'.$profil_website['background']);?>);" data-aos="fade" data-stellar-background-ratio="0.5">
      <div class="container">
        <div class="row align-items-center justify-content-center">

          <div class="col-md-12" data-aos="fade-up" data-aos-delay="400">
                        
            <div class="row mb-4">
              <div class="col-md-7">
                <h1><?php echo $profil_website['nama_website'] ?></h1>
                <p class="mb-5 lead"><?php echo $profil_website['tagline'] ?></p>
                <div>
                  <a href="#products-section" class="btn btn-white btn-outline-white py-3 px-5 rounded-0 mb-lg-0 mb-2 d-block d-sm-inline-block">Shop Now</a>
                  <a target="__blank" href="<?php echo $profil_website['instagram'] ?>" class="btn btn-white py-3 px-5 rounded-0 d-block d-sm-inline-block" >Follow Us On Instagram</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>  


    
    <div class="site-section" id="products-section">
      <div class="container">
        <div class="row mb-5 justify-content-center">
          <div class="col-md-6 text-center">
            <h3 class="section-sub-title">Popular Products</h3>
            <h2 class="section-title mb-3">Our Products</h2>
            <p>Produk yang kami tawarkan yaitu.</p>
          </div>
        </div>
        <div class="row product-ajax justify-content-center">
          
        </div>
      </div>
    </div>
    
    <div class="site-blocks-cover inner-page-cover overlay get-notification"  style="background-image: url(<?php echo base_url('include/setting_website/background/'.$profil_website['background']);?>); background-attachment: fixed;" data-aos="fade">
      <div class="container">

        <div class="row align-items-center justify-content-center">
          <form class="col-md-7" id="subscribe-form" method="post" action="frontend/subscriber">
            <h2>Get notified on each updates.</h2>
            <div class="d-flex mb-4">
              <input type="text" class="form-control rounded-0" name="email" placeholder="Enter your email address">
              <input type="submit" class="btn btn-white btn-outline-white rounded-0" value="Subscribe">
            </div>
            <p>Dapatkan berita dan produk terupdate dari kami dengan melakukan subscribe</p>
          </form>
        </div>

      </div>
    </div>

    

    <div class="site-section" id="about-section">
      <div class="container">
        <div class="row align-items-lg-center">
          <div class="col-md-8 mb-5 mb-lg-0 position-relative">
            <img src="<?php echo base_url('include/setting_website/img_about_us/'.$profil_website['img_about_us']);?>" class="img-fluid" alt="Image">
            <div class="experience">
              <span class="year"><?php echo substr($profil_website['nama_website'], 0, 500) ?></span>
              <span class="caption"><?php echo substr($profil_website['email'], 0, 500) ?></span>
            </div>
          </div>
          <div class="col-md-3 ml-auto">
            <h3 class="section-sub-title">Merchant Company</h3>
            <h2 class="section-title mb-3">About Us</h2>
            <p class="mb-4"><?php echo substr($profil_website['about_us'], 0, 500) ?> ... </p>
            <p><a href="#about-section" class="app-about btn btn-black btn-black--hover rounded-0">Learn More</a></p>
          </div>
        </div>
      </div>
    </div>


  <div class="site-blocks-cover overlay get-notification" id="special-section" style="background-image: url(<?php echo base_url('include/setting_website/background/'.$profil_website['background']);?>); background-attachment: fixed; background-position: top;" data-aos="fade">
      <div class="container">

        <div class="row align-items-center justify-content-center">
          <div class="col-md-7 text-center">
            <h3 class="section-sub-title">Special Event</h3>

            <?php if ($count_special_event>0){ ?>
                          <h3 class="section-title text-white mb-4"><?php echo $special_event->judul ?></h3>
                          <p class="mb-5 lead"><?php echo $special_event->deskripsi ?></p>
            <?php }else{ ?>
                          <h3 class="section-title text-white mb-4">Belum Ada Event</h3>
                          <p class="mb-5 lead">Ayo kunjungi terus website kami dan dapatkan event menarik dilain waktu</p>
            <?php } ?>
            <div id="date-countdown" data-time="<?php if($count_special_event>0){
             echo $special_event->sampai_tanggal;
            }else{
              echo date("Y/m/d");
            }; ?>" class="mb-5"></div>

            <p><a 
            <?php if($count_special_event>0){ ?>
             target="__blank" href="https://api.whatsapp.com/send?phone=62<?php echo $profil_website['no_hp']?>&text=<?php echo 'Halo Admin, saya ingin membeli barang melalui spesial event '.$special_event->judul; ?>"
            <?php }else{ ?>
              onclick ="alert('Belum ada event');"
            <?php }; ?>
               class="btn btn-white btn-outline-white py-3 px-5 rounded-0 mb-lg-0 mb-2 d-block d-sm-inline-block">Call Us to Get Special Event</a></p>
               <br>
          </div>
        </div>

      </div>
    </div>

    <div class="site-section bg-light" id="services-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h3 class="section-sub-title">Our Services</h3>
            <h2 class="section-title mb-3">We Offer Services</h2>
          </div>
        </div>
        <div class="row align-items-stretch">
          <?php foreach ($service as $key => $value): ?>
            
          <div class="col-md-6 col-lg-4 mb-4 mb-lg-4" data-aos="fade-up">
            <div class="unit-4 d-flex">
              <div class="unit-4-icon mr-4"><span class="text-primary icon-<?php echo $value->icon ?>"></span></div>
              <div>
                <h3><?php echo $value->judul ?></h3>
                <p><?php echo $value->text ?></p>
              </div>
            </div>
          </div>

          <?php endforeach ?>
        </div>
      </div>
    </div>

    <div class="site-section testimonial-wrap" id="testimonials-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-12 text-center">
            <h3 class="section-sub-title">People Says</h3>
            <h2 class="section-title mb-3">Testimonials</h2>
          </div>
        </div>
      </div>
      <div class="slide-one-item home-slider owl-carousel">
        <?php foreach ($testimoni as $key => $value): ?>
          <div>
            <div class="testimonial">
              <figure class="mb-4 d-block align-items-center justify-content-center">
                <div><img src="<?php echo base_url('include/testimoni/'.$value->foto);?>" alt="Image" class="w-100 img-fluid mb-3"></div>
              </figure>
              <blockquote class="mb-3">
                <p>&ldquo;<?php echo $value->testimoni ?>&rdquo;</p>
              </blockquote>
              <p class="text-black"><strong><?php echo $value->nama ?></strong></p>

              
            </div>
          </div>
          <?php endforeach ?>

        </div>
    </div>

    

 
    <div class="site-section bg-light" id="contact-section">
      <div class="container">
        <div class="row mb-5">
          <div class="col-md-12 text-center">
            <h3 class="section-sub-title">Contact Form</h3>
            <h2 class="section-title mb-3">Get In Touch</h2>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-12 text-center">
             <div class="map">
              <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3950.714731524058!2d112.60787611432906!3d-8.02832088236908!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e789d6670293ddf%3A0x394722089c7b330!2sJl.+Singo+Moyo%2C+Sonosari%2C+Kebonagung%2C+Kec.+Pakisaji%2C+Malang%2C+Jawa+Timur+65162!5e0!3m2!1sid!2sid!4v1561721990008!5m2!1sid!2sid" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
          </div>
        </div>
        <div class="row justify-content-center">
          <div class="col-md-7 mb-5">

            <form action="<?php echo base_url('frontend/save_email') ?>" method="POST"  id="email-form" class="p-5 bg-white">
              
              <h2 class="h4 text-black mb-5">Contact Form</h2> 

              <div class="row form-group">
                <div class="col-md-6 mb-3 mb-md-0">
                  <label class="text-black" for="fname">First Name</label>
                  <input type="text" id="fname" name="first_name" class="form-control rounded-0">
                </div>
                <div class="col-md-6">
                  <label class="text-black" for="lname">Last Name</label>
                  <input type="text" id="lname" name="last_name" class="form-control rounded-0">
                </div>
              </div>

              <div class="row form-group">
                
                <div class="col-md-12">
                  <label class="text-black" for="email">Email</label> 
                  <input type="email" id="email" name="email" class="form-control rounded-0">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="subject">Subject</label> 
                  <input type="subject" id="subject" name="subject" class="form-control rounded-0">
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <label class="text-black" for="message">Message</label> 
                  <textarea name="message" id="message" name="message" cols="30" rows="7" class="form-control rounded-0" placeholder="Write your notes or questions here..."></textarea>
                </div>
              </div>

              <div class="row form-group">
                <div class="col-md-12">
                  <input type="submit" value="Send Message" class=" btn btn-black rounded-0 py-3 px-4">
                </div>
              </div>

  
            </form>
          </div>
        
        </div>
        
      </div>
    </div>

  
    <footer class="site-footer bg-white">
      <div class="container">
       
        <div class="row">
          <div class="col-md-8">
            <div class="row">
              <div class="col-md-5">
                <h2 class="footer-heading mb-4">About Us</h2>
                <p><?php echo $profil_website['pemilik'] ?> | <?php echo $profil_website['no_hp'] ?> | <?php echo $profil_website['email'] ?></p>
              </div>
              <div class="col-md-3 ">
                <h2 class="footer-heading mb-4">Quick Links</h2>
                <ul class="list-unstyled">
                  <li><a href="#home-section" >Home</a></li>
                <li><a href="#products-section" >Products</a></li>
                <li><a href="#about-section" >About Us</a></li>
                <li><a href="#testimonials-section" >Testimonials</a></li>                
                <li><a href="#contact-section" >Contact</a></li>
                </ul>
              </div>
              <div class="col-md-4">
                <h2 class="footer-heading mb-4">Follow Us</h2>
                <a href="<?php echo $profil_website['facebook'] ?>" class="pl-0 pr-3"><span class="icon-facebook"></span></a>
                <a href="<?php echo $profil_website['twitter'] ?>" class="pl-3 pr-3"><span class="icon-twitter"></span></a>
                <a href="<?php echo $profil_website['instagram'] ?>" class="pl-3 pr-3"><span class="icon-instagram"></span></a>
                <a href="<?php echo $profil_website['youtube'] ?>" class="pl-3 pr-3"><span class="icon-youtube"></span></a>
              </div>
            </div>
          </div>
          <div class="col-md-3 ml-auto">
            <?php echo $profil_website['pemilik'] ?>  +62<?php echo $profil_website['no_hp'] ?>  <?php echo $profil_website['email'] ?>
          </div>
        </div>
        <div class="row pt-5 mt-5 text-center">
          <div class="col-md-12">
            <div class="border-top pt-5">
            <p>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
        Copyright &copy;<script>document.write(new Date().getFullYear());</script> All rights reserved | This template is made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
        <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
      </p>
            </div>
          </div>
          
        </div>
      </div>
    </footer>

  </div> <!-- .site-wrap -->

<div class="modal" id="sdf" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Preview</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class ="modal-content-ajax"></div>

      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="readmore-about" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-xl" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Preview</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <?php echo $profil_website['about_us'] ?>

      </div>
      <div class="modal-footer">
        
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


<a target="__blank" href="https://api.whatsapp.com/send?phone=62<?php echo $profil_website['no_hp']?>&text=<?php echo $profil_website['wa_text']; ?>" class="float">
    <i class="fa fa-whatsapp my-float"></i>
</a>
  <script src="<?php echo base_url('include/template/selling/js/');?>jquery-3.3.1.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>jquery-migrate-3.0.1.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>jquery-ui.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>popper.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>bootstrap.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>owl.carousel.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>jquery.stellar.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>jquery.countdown.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>bootstrap-datepicker.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>jquery.easing.1.3.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>aos.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>jquery.fancybox.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>jquery.sticky.js"></script>
  <?php $this->load->view('role/frontend/selling/js_selling') ?>
  
    <script src="<?php echo base_url('include/sweetalert2/dist/');?>sweetalert2.min.js"></script>
  <script src="<?php echo base_url('include/template/selling/js/');?>main.js"></script>
    
<script src="<?php echo base_url('include/template/toastr/toastr.min.js')?>"></script>
<script src="<?php echo base_url('include/core/core_shop.js')?>"></script>
  </body>
</html>