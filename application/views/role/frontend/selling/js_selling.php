<script type="text/javascript">
	 
        var siteCountDown = function(tanggal) {

        $('#date-countdown').countdown(tanggal, function(event) {
          var $this = $(this).html(event.strftime(''
            + '<span class="countdown-block"><span class="label">%w</span> weeks </span>'
            + '<span class="countdown-block"><span class="label">%d</span> days </span>'
            + '<span class="countdown-block"><span class="label">%H</span> hr </span>'
            + '<span class="countdown-block"><span class="label">%M</span> min </span>'
            + '<span class="countdown-block"><span class="label">%S</span> sec</span>'));
        });
                
    };
     $(document.body).on('submit', '#email-form' ,function(e){
           
            e.preventDefault();
            $.ajax({
                url : $(this).attr('action') || window.location.pathname,
                type: "POST",
                data:$(this).serialize(),
                success: function (data) {
                   
                    Swal.fire({
                        title: 'Email Terkirim',
                        text: 'Terimakasih telah mengirim pesan kepada kami, untuk balasan akan kami whatsapp atau kami kirim email balasan',
                        type: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                      }).then((result) => {
                        if (result.value) {
                         location.reload();
                        }
                      })
                    

                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
     }); 

     $(document.body).on('submit', '#subscribe-form' ,function(e){
           
            e.preventDefault();
            $.ajax({
                url : $(this).attr('action') || window.location.pathname,
                type: "POST",
                data: $(this).serialize(),
                success: function (data) {
                   
                    Swal.fire({
                        title: 'Berhasil Subscribe',
                        text: 'Terimakasih mengikuti kami, kami akan mengirimkan berita, produk dan servis terbaru kami ke email yang anda lampirkan',
                        type: 'success',
                        confirmButtonColor: '#3085d6',
                        confirmButtonText: 'OK'
                      }).then((result) => {
                        if (result.value) {
                         location.reload();
                        }
                      })
                    

                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            return false;
     }); 

    $( document ).ready(function() {
      get_katalog();
      siteCountDown($("#date-countdown").data('time'));
      
    });

   

    function get_katalog(halaman=1,start=0,end=0,limit=6)
      {
        $.ajax({
              url: "<?php echo base_url().'/frontend/get_product'?>",
              type: "POST",
              data:{
                page:halaman,
                start:start,
                end:end,
                limit:limit
              },
              dataType: "html",
              success: function (data) {
                $('.product-ajax').focus();
                $('.product-ajax').html(data);
              },
              error: function (xhr, ajaxOptions, thrownError) {
                swal("Error deleting!", "Please try again", "error");
              }
          });
      }

      $(document.body).on('click', '.app-view' ,function(e){

            e.preventDefault();
            $.ajax({
                url : $(this).data('url') || window.location.pathname,
                type: "POST",
                data:{id:$(this).data('id')},
                success: function (data) {
                    $('.modal-content-ajax').html(data);
                    $('#sdf').modal('show'); 
                },
                error: function (jXHR, textStatus, errorThrown) {
                    alert(errorThrown);
                }
            });
            // return false;
     }); 

      $(document.body).on('click', '.app-wa' ,function(e){
            var message = window.encodeURIComponent($('.app-wa').data('text'));
            var phone   =   $('.app-wa').data('phone');
            var whatsapp_url = "https://api.whatsapp.com/send?phone="+phone+"&text=" + message;
            window.location.href = whatsapp_url;
     }); 


      $(document.body).on('click', '.app-about' ,function(e){
                    $('#readmore-about').modal('show'); 
                    $('#about-section').focus();
     }); 
   
</script>