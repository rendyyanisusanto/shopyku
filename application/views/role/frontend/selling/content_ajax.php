<div class="bg-white py-4 mb-4">
          <div class="row mx-4 my-4 product-item-2 align-items-start">
            <div class="col-md-4 mb-5 mb-md-0">
              <img src="<?php echo base_url('include/foto_produk/'.$produk_get->foto);?>" alt="Image" class="img-fluid">
            </div>
           
            <div class="col-md-8 ml-auto product-title-wrap">
              <span class="number">01.</span>
              <h3 class="text-black mb-4 font-weight-bold"><?php echo $produk_get->nama_barang ?></h3>
              <p class="mb-4"><?php echo $produk_get->deskripsi ?></p>
              
              
              <div class="mb-4"> 
                <h3 class="text-black font-weight-bold h5">Price:</h3>
                <div class="price"><?php echo $harga_produk ?></div>
              </div>
              <p>
                <a href="#" data-link="frontend/add_cart" data-harga_barang="<?php echo $produk_get->harga_barang ?>"  data-nama_barang="<?php echo $produk_get->nama_barang ?>" data-img="<?php echo base_url('include/foto_produk/'.$produk_get->foto);?>" data-id_barang="<?php echo $produk_get->id_barang ?>" data-weight="<?php echo $produk_get->berat_barang ?>" class="btn app-cart btn-black btn-outline-black rounded-0 d-block mb-2 mb-lg-0 d-lg-inline-block">Masukan Keranjang</a>
                <a data-text="<?php echo 'Halo Admin, Saya ingin membeli produk '.$produk_get->nama_barang.' Dengan Jumlah .... (isi jumlah)...,atas Nama : ....(isi Nama).. dengan alamat Lengkap : ..(isi alamat lengkap)...'; ?>"
                 href="#" data-phone="62<?php echo $profil_website['no_hp']?>" class="app-wa btn btn-black rounded-0 d-block d-lg-inline-block">Beli Sekarang Lewat WA</a>
              </p>
            </div>
          </div>
</div>
