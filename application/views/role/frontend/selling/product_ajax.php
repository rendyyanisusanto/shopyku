
          <?php foreach ($produk as $key => $value): ?>
          <div class="col-lg-4 col-md-6 mb-5">
            <div class="product-item">
              <figure>
                <img src="<?php echo base_url('include/foto_produk/'.$value->foto);?>" alt="Image" class="img-fluid">
              </figure>
              <div class="px-4">
                <h3><a href="#"><?php echo $value->nama_barang ?></a></h3>
                <div class="mb-3">
                  <span class="meta-icons mr-3"><a href="#" class="mr-2"><span class="icon-cubes text-warning"></span></a> <?php echo $value->stok ?></span>
                  <span class="meta-icons wishlist"><a href="#" class="mr-2"><span class="icon-heart"></span></a> 29</span>
                </div>
                <p class="mb-4">Klik View, untuk melihat deskripsi produk</p>
                <div>
                  <a href="#" class="btn btn-black mr-1 rounded-0">Rp. <?php echo number_format($value->harga_barang,0,'.','.') ?></a>
                  <a href="#" data-id="<?php echo $value->id_barang ?>" data-url="<?php echo base_url('frontend/preview'); ?>" class="app-view btn btn-black btn-outline-black ml-1 rounded-0">View</a>
                </div>
              </div>
            </div>
          </div>
          <?php endforeach ?>

       <div class="row align-items-center justify-content-center">
            <center><?php echo $pagination ?></center>
       </div>