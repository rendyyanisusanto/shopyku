<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $profil_website['nama_website'] ?></title>

  <!-- Bootstrap -->
  <link href="<?php echo base_url('include/template/resi/css/');?>bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url('include/template/resi/css/');?>responsive-slider.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('include/template/resi/css/');?>animate.css">
  <link rel="stylesheet" href="<?php echo base_url('include/template/resi/css/');?>font-awesome.min.css">
  <link href="<?php echo base_url('include/template/resi/css/');?>style.css" rel="stylesheet">
  <!-- =======================================================
    Theme Name: Resi
    Theme URL: https://bootstrapmade.com/resi-free-bootstrap-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <header>
    <div class="container">
      <div class="row">
        <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <div class="navbar-brand">
                <a href="index.html"><h1><?php echo $profil_website['nama_website'] ?> </h1></a>
              </div>
            </div>
            <div class="menu">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" ><a href="<?php echo base_url('frontend/') ?>">Home</a></li>
                <li role="presentation" ><a href="<?php echo base_url('frontend/about_us') ?>">About Us</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/katalog') ?>">Katalog</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/blog') ?>">Blog</a></li>
                <li role="presentation" ><a href="<?php echo base_url('frontend/kegiatan') ?>">Kegiatan</a></li>
                <li role="presentation" class="active"><a href="<?php echo base_url('frontend/kontak') ?>">Contact</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>

  <div class="breadcrumb">
    <h4>Kegiatan</h4>
  </div>

  <div class="container">
   <div class="col-lg-6">
      <div id="sendmessage">Your message has been sent. Thank you!</div>
      <div id="errormessage"></div>
      <form action="" method="post" role="form" class="contactForm">
        <div class="form-group">
          <input type="text" name="name" class="form-control" id="name" placeholder="Your Name" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
          <div class="validation"></div>
        </div>
        <div class="form-group">
          <input type="email" class="form-control" name="email" id="email" placeholder="Your Email" data-rule="email" data-msg="Please enter a valid email" />
          <div class="validation"></div>
        </div>
        <div class="form-group">
          <input type="text" class="form-control" name="subject" id="subject" placeholder="Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
          <div class="validation"></div>
        </div>
        <div class="form-group">
          <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Message"></textarea>
          <div class="validation"></div>
        </div>

        <div class="text-center"><button type="submit" class="btn btn-default">Send Message</button></div>
      </form>

    </div>
    <div class="col-lg-6">
      <div class="map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d22864.11283411948!2d-73.96468908098944!3d40.630720240038435!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c24fa5d33f083b%3A0xc80b8f06e177fe62!2sNew+York%2C+NY%2C+USA!5e0!3m2!1sen!2sbg!4v1540447494452" width="100%" height="380" frameborder="0" style="border:0" allowfullscreen></iframe>
      </div>
    </div>
  </div>


  <hr>

  <!--start footer-->
  <footer>
   
    <div class="sub-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <ul class="social-network">
              <li><a href="<?php echo $profil_website['facebook'] ?>" data-placement="top" title="Facebook"><i class="fa fa-facebook fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['twitter'] ?>" data-placement="top" title="Twitter"><i class="fa fa-twitter fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['instagram'] ?>" data-placement="top" title="Instagram"><i class="fa fa-instagram fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['youtube'] ?>" data-placement="top" title="Youtube"><i class="fa fa-youtube fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['email'] ?>" data-placement="top" title="Google Mail"><i class="fa fa-google-mail fa-1x"></i></a></li>
            </ul>
          </div>
          <div class="col-lg-12">
            <div class="copyright">
              <p>&copy;Myber Center - All right reserved.</p>
              <div class="credits">
                <!--
                  All the links in the footer should remain intact. 
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Resi
                -->
                Created by <a href="https://myber.co.id/">Myber</a>
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </footer>
  <!--end footer-->

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="<?php echo base_url('include/template/resi/js/');?>jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?php echo base_url('include/template/resi/js/');?>bootstrap.min.js"></script>
  <script src="<?php echo base_url('include/template/resi/js/');?>responsive-slider.js"></script>
  <script src="<?php echo base_url('include/template/resi/js/');?>wow.min.js"></script>

  <script src="<?php echo base_url('include/template/resi/contactform');?>/contactform.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>


</body>

</html>
