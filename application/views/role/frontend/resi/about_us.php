<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $profil_website['nama_website'] ?></title>

  <!-- Bootstrap -->
  <link href="<?php echo base_url('include/template/resi/css/');?>bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url('include/template/resi/css/');?>responsive-slider.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('include/template/resi/css/');?>animate.css">
  <link rel="stylesheet" href="<?php echo base_url('include/template/resi/css/');?>font-awesome.min.css">
  <link href="<?php echo base_url('include/template/resi/css/');?>style.css" rel="stylesheet">
  <!-- =======================================================
    Theme Name: Resi
    Theme URL: https://bootstrapmade.com/resi-free-bootstrap-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <header>
    <div class="container">
      <div class="row">
        <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <div class="navbar-brand">
                <a href="index.html"><h1><?php echo $profil_website['nama_website'] ?> </h1></a>
              </div>
            </div>
            <div class="menu">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" ><a href="<?php echo base_url('frontend/') ?>">Home</a></li>
                <li role="presentation" class="active"><a href="<?php echo base_url('frontend/about_us') ?>">About Us</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/katalog') ?>">Katalog</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/blog') ?>">Blog</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/kegiatan') ?>">Kegiatan</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/kontak') ?>">Contact</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>




  <div class="content">
    <p><?php echo $profil_website['about_us'] ?> </p>
  </div>



  <hr>

   <footer>
   
    <div class="sub-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <ul class="social-network">
              <li><a href="<?php echo $profil_website['facebook'] ?>" data-placement="top" title="Facebook"><i class="fa fa-facebook fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['twitter'] ?>" data-placement="top" title="Twitter"><i class="fa fa-twitter fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['instagram'] ?>" data-placement="top" title="Instagram"><i class="fa fa-instagram fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['youtube'] ?>" data-placement="top" title="Youtube"><i class="fa fa-youtube fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['email'] ?>" data-placement="top" title="Google Mail"><i class="fa fa-google-mail fa-1x"></i></a></li>
            </ul>
          </div>
          <div class="col-lg-12">
            <div class="copyright">
              <p>&copy;Myber Center - All right reserved.</p>
              <div class="credits">
                <!--
                  All the links in the footer should remain intact. 
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Resi
                -->
                Created by <a href="https://myber.co.id/">Myber</a>
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </footer>
  <!--end footer-->

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="<?php echo base_url('include/template/resi/js/');?>jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?php echo base_url('include/template/resi/js/');?>bootstrap.min.js"></script>
  <script src="<?php echo base_url('include/template/resi/js/');?>responsive-slider.js"></script>
  <script src="<?php echo base_url('include/template/resi/js/');?>wow.min.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>

</body>

</html>
