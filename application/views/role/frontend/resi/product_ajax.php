<?php foreach ($produk as $key => $value): ?>
<div class="col-md-4">

  <div class="panel panel-default">
    <div class="panel-heading"><?php echo $value->nama_barang ?></div>
    <div class="panel-body">

            <img  src="<?php echo base_url('include/foto_produk/'.$value->foto);?>" alt="Image" class="img-responsive">
            
    </div>
    <div class="panel-heading"><a class="btn btn-warning btn-block app-view" data-id="<?php echo $value->id_barang ?>" data-url="<?php echo base_url('frontend/preview'); ?>">Deskripsi</a></div>
  </div>
         
</div>
        
<?php endforeach ?>

<div class="row align-items-center justify-content-center">
            <center><?php echo $pagination ?></center>
       </div>