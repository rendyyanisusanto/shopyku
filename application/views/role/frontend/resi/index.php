<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $profil_website['nama_website'] ?></title>

  <!-- Bootstrap -->
  <link href="<?php echo base_url('include/template/resi/css/');?>bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url('include/template/resi/css/');?>responsive-slider.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('include/template/resi/css/');?>animate.css">
  <link rel="stylesheet" href="<?php echo base_url('include/template/resi/css/');?>font-awesome.min.css">
  <link href="<?php echo base_url('include/template/resi/css/');?>style.css" rel="stylesheet">
  <!-- =======================================================
    Theme Name: Resi
    Theme URL: https://bootstrapmade.com/resi-free-bootstrap-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <header>
    <div class="container">
      <div class="row">
        <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <div class="navbar-brand">
                <a href="index.html"><h1><?php echo $profil_website['nama_website'] ?> </h1></a>
              </div>
            </div>
            <div class="menu">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" class="active"><a href="<?php echo base_url('frontend/') ?>">Home</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/about_us') ?>">About Us</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/katalog') ?>">Katalog</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/blog') ?>">Blog</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/kegiatan') ?>">Kegiatan</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/kontak') ?>">Contact</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>

  <div class="slider">
    <div id="about-slider">
      <div id="carousel-slider" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators visible-xs">
          <li data-target="#carousel-slider" data-slide-to="0" class="active"></li>
          <li data-target="#carousel-slider" data-slide-to="1"></li>
          <li data-target="#carousel-slider" data-slide-to="2"></li>
        </ol>

        <div class="carousel-inner">
          <div class="item active">
            <img src="<?php echo base_url('include/setting_website/background/'.$profil_website['background']);?>" class="img-responsive" alt="">
            <div class="carousel-caption">
              <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.3s">
                <h2><span><?php echo $profil_website['nama_website'] ?></span></h2>
              </div>
              <div class="col-md-10 col-md-offset-1">
                <div class="wow fadeInUp" data-wow-offset="0" data-wow-delay="0.6s">
                  <p><?php echo $profil_website['tagline'] ?></p>
                </div>
              </div>
             
            </div>
          </div>

          
        </div>

      </div>
      <!--/#carousel-slider-->
    </div>
    <!--/#about-slider-->
  </div>
  <!--/#slider-->



  <div class="content">
    <p><?php echo substr($profil_website['about_us'], 0, 300) ?>...(READ MORE) </p>
  </div>
  <div class="breadcrumb">
    <h4>Recent Works</h4>
  </div>

  <div class="container">
    <div class="row">
      <div class="boxs">
        <?php foreach ($service as $key => $value): ?>
          
        

        <div class="col-md-4">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="0.8s">
            <div class="align-center"  style="background: #ecf0f1;">
              <h5><?php echo $value->judul ?></h5>
              <div class="icon">
                <i class="fa fa-heart-o fa-3x"></i>
              </div>
              <p>

                <?php echo substr($value->text, 0, 50) ?>...
              </p>
              
            </div>
          </div>
        </div>

        <?php endforeach ?>


      </div>
    </div>
  </div>


  <div class="breadcrumb">
    <h4>Keunggulan Kami</h4>
  </div>

  <div class="container">
    <div class="row">
      <div class="">
        <div class="col-md-7">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="0.8s">
            <img src="<?php echo base_url('include/template/resi/img/');?>2.png" alt="" class="img-responsive">
          </div>
        </div>
        <div class="col-md-5">
          <h2>Visi</h2>
            <p>
              <?php if ($visi->num_rows()>1){ ?>
                <ol>
                  <?php foreach ($visi->result() as $key => $value): ?>
                    <li><?php echo $value->nama_visi ?></li>
                  <?php endforeach ?>
                </ol>
              <?php }else{ ?>
                  <?php echo $visi->row()->nama_visi ?>
              <?php } ?>
            </p>

            <br>
            <h2>Misi</h2>
            <p>
              <?php if ($misi->num_rows()>1){ ?>
                <ol>
                  <?php foreach ($misi->result() as $key => $value): ?>
                    <li><?php echo $value->nama_misi ?></li>
                  <?php endforeach ?>
                </ol>
              <?php }else{ ?>
                  <?php echo $misi->row()->nama_misi ?>
              <?php } ?>
            </p>

           
        </div>
      </div>
    </div>
  </div>

  <div class="breadcrumb">
    <h4>Keunggulan Kami</h4>
  </div>

  <div class="container">
    <div class="row">
      <div class="">
        <div class="col-md-4">
          <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="0.8s">
            <div class="list-group">
              <?php foreach ($keunggulan as $key => $value): ?>
              <div class="list-group-item">
                <h4 class="list-group-item-heading"><?php echo $value->nama_keunggulan ?></h4>
                <p class="list-group-item-text"><?php echo $value->text ?>
                </p>
              </div>
              <?php endforeach ?>
              
            </div>
          </div>
        </div>
        <div class="col-md-8">
          <div class="image">
            <div class="wow bounceIn" data-wow-offset="0" data-wow-delay="1.3s">
              <img src="<?php echo base_url('include/template/resi/img/');?>5.jpg" alt="" class="img-responsive">
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <hr>

  <!--start footer-->
   <footer>
   
    <div class="sub-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <ul class="social-network">
              <li><a href="<?php echo $profil_website['facebook'] ?>" data-placement="top" title="Facebook"><i class="fa fa-facebook fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['twitter'] ?>" data-placement="top" title="Twitter"><i class="fa fa-twitter fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['instagram'] ?>" data-placement="top" title="Instagram"><i class="fa fa-instagram fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['youtube'] ?>" data-placement="top" title="Youtube"><i class="fa fa-youtube fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['email'] ?>" data-placement="top" title="Google Mail"><i class="fa fa-google-mail fa-1x"></i></a></li>
            </ul>
          </div>
          <div class="col-lg-12">
            <div class="copyright">
              <p>&copy;Myber Center - All right reserved.</p>
              <div class="credits">
                <!--
                  All the links in the footer should remain intact. 
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Resi
                -->
                Created by <a href="https://myber.co.id/">Myber</a>
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </footer>
  <!--end footer-->

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="<?php echo base_url('include/template/resi/js/');?>jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?php echo base_url('include/template/resi/js/');?>bootstrap.min.js"></script>
  <script src="<?php echo base_url('include/template/resi/js/');?>responsive-slider.js"></script>
  <script src="<?php echo base_url('include/template/resi/js/');?>wow.min.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>

</body>

</html>
