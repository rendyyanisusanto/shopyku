<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title><?php echo $profil_website['nama_website'] ?></title>

  <!-- Bootstrap -->
  <link href="<?php echo base_url('include/template/resi/css/');?>bootstrap.min.css" rel="stylesheet">
  <link href="<?php echo base_url('include/template/resi/css/');?>responsive-slider.css" rel="stylesheet">
  <link rel="stylesheet" href="<?php echo base_url('include/template/resi/css/');?>animate.css">
  <link rel="stylesheet" href="<?php echo base_url('include/template/resi/css/');?>font-awesome.min.css">
  <link href="<?php echo base_url('include/template/resi/css/');?>style.css" rel="stylesheet">
  <!-- =======================================================
    Theme Name: Resi
    Theme URL: https://bootstrapmade.com/resi-free-bootstrap-html-template/
    Author: BootstrapMade
    Author URL: https://bootstrapmade.com
  ======================================================= -->
</head>

<body>
  <header>
    <div class="container">
      <div class="row">
        <nav class="navbar navbar-default" role="navigation">
          <div class="container-fluid">
            <div class="navbar-header">
              <div class="navbar-brand">
                <a href="index.html"><h1><?php echo $profil_website['nama_website'] ?> </h1></a>
              </div>
            </div>
            <div class="menu">
              <ul class="nav nav-tabs" role="tablist">
                <li role="presentation" ><a href="<?php echo base_url('frontend/') ?>">Home</a></li>
                <li role="presentation" ><a href="<?php echo base_url('frontend/about_us') ?>">About Us</a></li>
                <li role="presentation"><a href="<?php echo base_url('frontend/katalog') ?>">Katalog</a></li>
                <li role="presentation" class="active"><a href="<?php echo base_url('frontend/blog') ?>">Blog</a></li>
                <li role="presentation" ><a href="<?php echo base_url('frontend/kegiatan') ?>">Kegiatan</a></li>
                <li role="presentation" ><a href="<?php echo base_url('frontend/kontak') ?>">Contact</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </div>
    </div>
  </header>

  <div class="breadcrumb">
    <h4>Kegiatan</h4>
  </div>

  
  <div class="container">
    <div class="row">
      <div class="col-md-8">
        <div class="page-header">
          <div class="blog">
            <h5>January,15 2014</h5>
            <img src="<?php echo base_url('include/template/resi/img/');?>4.jpg" class="img-responsive" alt="" />

            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam,quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
              aliquip ex ea commodo consequat.</p>

            <h3>Lorem ipsum dolor sit amet</h3>

            <p>Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat, vel illum dolore eu feugiat nulla facilisis at vero eros et accumsan et iusto odio dignissim qui blandit praesent luptatum zzril delenit augue duis dolore
              te feugait nulla facilisi. Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum claritatem.</p>

            <p>Nam liber tempor cum soluta nobis eleifend option congue nihil imperdiet doming id quod mazim placerat facer possim assum. Typi non habent claritatem insitam; est usus legentis in iis qui facit eorum.</p>

            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut
              aliquip ex ea commodo consequat.</p>
            <div class="ficon">
              <a href="" alt="">Learn more</a>
            </div>
          </div>
        </div>
        <div class="container">
          <div class="row">
            <nav>
              <ul class="pagination">
                <li><a href="#"><span aria-hidden="true">&laquo;</span><span class="sr-only">Previous</span></a></li>
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
                <li><a href="#"><span aria-hidden="true">&raquo;</span><span class="sr-only">Next</span></a></li>
              </ul>
            </nav>
          </div>
        </div>
      </div>




      <div class="col-md-4">
        <form class="form-search">
          <input class="form-control" type="text" placeholder="Search..">
        </form>
        <div class="panel panel-default">
          <div class="panel-heading">
            <strong>Popular Posts</strong>
          </div>
          <div class="panel-body">
            <div class="media">
              <a class="media-left" href="#">
                <img src="<?php echo base_url('include/template/resi/img/');?>b.jpg" alt="">
              </a>
              <div class="media-body">
                <h4 class="media-heading">Kelly Hidayah</h4>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                </p>
                <div class="ficon">
                  <a href="" alt="">Learn more</a>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="media">
              <a class="media-left" href="#">
                <img src="<?php echo base_url('include/template/resi/img/');?>a.jpg" alt="">
              </a>
              <div class="media-body">
                <h4 class="media-heading">Kelly Hidayah</h4>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                </p>
                <div class="ficon">
                  <a href="" alt="">Learn more</a>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="media">
              <a class="media-left" href="#">
                <img src="<?php echo base_url('include/template/resi/img/');?>c.jpg" alt="">
              </a>
              <div class="media-body">
                <h4 class="media-heading">Kelly Hidayah</h4>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                </p>
                <div class="ficon">
                  <a href="" alt="">Learn more</a>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="media">
              <a class="media-left" href="#">
                <img src="<?php echo base_url('include/template/resi/img/');?>d.jpg" alt="">
              </a>
              <div class="media-body">
                <h4 class="media-heading">Kelly Hidayah</h4>
                <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore
                </p>
                <div class="ficon">
                  <a href="" alt="">Learn more</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <div class="container">
    <div class="row">
      <div class="col-md-8">
        
      </div>
      <div class="col-md-4">
        <div class="popular-tags">
          <h5>Popular tags</h5>
          <ul class="tags">
            <li><a href="#">Web design</a></li>
            <li><a href="#">Trends</a></li>
            <li><a href="#">Technology</a></li>
            <li><a href="#">Internet</a></li>
            <li><a href="#">Tutorial</a></li>
            <li><a href="#">Development</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>

  <hr>

  <!--start footer-->
  <footer>
   
    <div class="sub-footer">
      <div class="container">
        <div class="row">
          <div class="col-lg-12">
            <ul class="social-network">
              <li><a href="<?php echo $profil_website['facebook'] ?>" data-placement="top" title="Facebook"><i class="fa fa-facebook fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['twitter'] ?>" data-placement="top" title="Twitter"><i class="fa fa-twitter fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['instagram'] ?>" data-placement="top" title="Instagram"><i class="fa fa-instagram fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['youtube'] ?>" data-placement="top" title="Youtube"><i class="fa fa-youtube fa-1x"></i></a></li>
              <li><a href="<?php echo $profil_website['email'] ?>" data-placement="top" title="Google Mail"><i class="fa fa-google-mail fa-1x"></i></a></li>
            </ul>
          </div>
          <div class="col-lg-12">
            <div class="copyright">
              <p>&copy;Myber Center - All right reserved.</p>
              <div class="credits">
                <!--
                  All the links in the footer should remain intact. 
                  You can delete the links only if you purchased the pro version.
                  Licensing information: https://bootstrapmade.com/license/
                  Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/buy/?theme=Resi
                -->
                Created by <a href="https://myber.co.id/">Myber</a>
                Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
              </div>
            </div>
          </div>

        </div>
      </div>
    </div>
  </footer>
  <!--end footer-->

  <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
  <script src="<?php echo base_url('include/template/resi/js/');?>jquery-2.1.1.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="<?php echo base_url('include/template/resi/js/');?>bootstrap.min.js"></script>
  <script src="<?php echo base_url('include/template/resi/js/');?>responsive-slider.js"></script>
  <script src="<?php echo base_url('include/template/resi/js/');?>wow.min.js"></script>

  <script src="<?php echo base_url('include/template/resi/contactform');?>/contactform.js"></script>
  <script>
    wow = new WOW({}).init();
  </script>


</body>

</html>
