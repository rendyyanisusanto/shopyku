
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $profil_website['nama_website'] ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/dist')?>/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/dist')?>/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url('include/core/core.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('include/template/toastr/toastr.css')?>">
  <link rel="icon" href="<?php echo base_url('include/setting_website/'.$profil_website['icon'])?>">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
  <script src="<?php echo base_url('include/template/adminlte/bower_components')?>/jquery/dist/jquery.min.js"></script>
  
<script src="<?php echo base_url('include/core/core.js')?>"></script>
  
</head>
<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">
      <div class="content-wrapper">
        <div class="container">
          <div class="row">

            <section class="content">
              <div class="row">
                <center><b><h2>Formulir Pemesanan</h2></b></center>
                <hr>
                <br>
              </div>
              <form class="form-horizontal" id="frm">
              <div class="row">
                <div class="col-md-6">
                  <?php //print_r(json_decode($jenis_pengiriman->)) ?>
                  <div class="box box-navy box-solid">
                    <div class="box-header bg-navy with-border">
                      <h3 class="box-title"><i class="fa fa-user"></i> Identitas Pemesan</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Nama</label>

                          <div class="col-sm-10">
                            <input type="text" name="nama" class="form-control" id="nama" placeholder="Mohon tulis nama lengkap">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">No HP/WA</label>

                          <div class="col-sm-10">
                            <input type="text" name="no_hp" class="form-control" id="no_hp" placeholder="No HP/ Whatsapp">
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputEmail3"  class="col-sm-2 control-label">Email</label>

                          <div class="col-sm-10">
                            <input type="email" name="email" class="form-control" id="email" placeholder="Mohon tulis Email (jika ada)">
                          </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                  </div>

                  <div class="box box-default box-solid">
                    <div class="box-header bg-navy with-border">
                      <h3 class="box-title"><i class="fa fa-home"></i>Alamat Pengiriman</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Provinsi</label>

                          <div class="col-sm-10">
                            <select class="form-control" name="provinsi" id="province">
                              <option value="">--Pilih--</option>
                            </select>
                          </div>
                        </div>
                       <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Kota</label>

                          <div class="col-sm-10">
                            <select class="form-control" id="city" name="kota">
                              <option value="">--Pilih--</option>
                            </select>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Alamat Lengkap</label>

                          <div class="col-sm-10">
                            <textarea class="form-control" name="alamat" id="alamat"></textarea>
                          </div>
                        </div>

                        <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Kode Pos</label>

                          <div class="col-sm-10">
                            <input type="text" class="form-control" id="kode_pos" name="kode_pos" >
                          </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <div class="box box-default box-solid">
                    <div class="box-header bg-navy with-border">
                      <h3 class="box-title"><i class="fa fa-truck"></i> Jenis Pengiriman</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Pilih Ekspedisi</label>

                          <div class="col-sm-10">
                            <select class="form-control" id="ekspedisi" name="ekspedisi">
                              <option value=""> --Pilih--</option>
                              <?php foreach (json_decode($jenis_pengiriman['value_add']) as $key => $value): ?>
                                <option value="<?php echo $value->val ?>"><?php echo $value->text ?></option>
                              <?php endforeach ?>
                            </select>
                          </div>
                        </div>

                        <div class="opsi-ekspedisi"></div>

                       
                    </div>
                    <!-- /.box-body -->
                  </div>

                  <div class="box box-default box-solid">
                    <div class="box-header bg-navy with-border">
                      <h3 class="box-title"><i class="fa fa-credit-card"></i> Jenis Pembayaran</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <div class="form-group">
                          <label for="inputEmail3" class="col-sm-2 control-label">Metode Pembayaran</label>

                          <div class="col-sm-10">
                            <select class="form-control" id="jenis_pembayaran">
                              <option value=""> --Pilih--</option>
                              <?php foreach (json_decode($jenis_pembayaran['value_add']) as $key => $value): ?>
                                <option data-rek="<?php echo $value->no_rek ?>" data-an="<?php echo $value->atas_nama ?>" value="<?php echo $value->text ?>"><?php echo $value->text ?></option>
                              <?php endforeach ?>
                            </select>
                          </div>
                        </div>

                        <br>

                        <b><h3 class="rk"></h3></b>

                       
                    </div>
                    <!-- /.box-body -->
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="box box-default box-solid">
                    <div class="box-header bg-navy with-border">
                      <h3 class="box-title"><i class="fa fa-cube"></i>Barang yg dibeli</h3>

                      <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                      </div>
                      <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                       <table class="table table-bordered" >
                         <tr>
                            <th></th>
                           <th>Produk</th>
                           <th>Berat</th>
                           <th>Harga</th>
                           <th width="20%">QTY</th>
                         </tr>
                       <?php foreach ($items as $key => $value): ?>
                         <tr >
                            <td><img src="<?php echo $value['img'];?>" style="max-width: 100px;" alt="Image" class="img-fluid"></td>
                           <td style="vertical-align: middle;"><?php echo $value['name'] ?></td>
                           <td style="vertical-align: middle;"><?php echo $value['weight'] ?> Gram</td>
                           <td style="vertical-align: middle;">Rp. <?php echo number_format($value['price'],0,',','.') ?></td>
                           <td style="vertical-align: middle;"><input type="number" class="form-control qty" name="qty" data-rowid="<?php echo $value['rowid'] ?>" value="<?php echo $value['qty'] ?>"></td>
                         </tr>
                       <?php endforeach ?>

                       </table>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <div class="box box-default box-solid">
                    
                    <div class="box-body">
                         <div class="total">
                        </div>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <div class="box box-default box-solid">
                    
                    <div class="box-body">
                       <a href="<?php echo base_url(); ?>" class="btn btn-warning"><i class="fa fa-arrow-left"></i> Belanja Lagi</a>
                       <button class="btn btn-success pull-right proceed" type="button"><i class="fa fa-check-square"></i> Proses Transaksi</button>
                    </div>
                    <!-- /.box-body -->
                  </div>

                </div>
              </div>
              </form>
            </section>
          </div>
        </div>
      </div>
  </div>
<!-- /.register-box -->
</body>

<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/adminlte.min.js"></script>
<!--  -->
<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/demo.js"></script>

<script src="<?php echo base_url('include/template/toastr/toastr.min.js')?>"></script>

<script src="<?php echo base_url('include/core/core_shop.js')?>"></script>
<script type="text/javascript">
  $(document).ready(function(e){
    set_province();
    get_total();
    $("#province").on("change", function(e){
        send = $(this).val();
        
        send_ajax( "<?php echo base_url('Api/get_city') ?>",{id_province:send} ).then( function(data){
            $("#city").html(data);
            console.log(data);
        });
    });

    $('.proceed').on('click',function(){
        var data = {
          nama            : $('#nama').val(),
          no_hp           : $('#no_hp').val(),
          email           : $('#email').val(),
          provinsi        : $('#province option:selected').text(),
          kota            : $('#city option:selected').text(),
          kode_pos        : $('#kode_pos').val(),
          alamat          : $('#alamat').val(),
          ekspedisi       : $('#ekspedisi option:selected').text(),
          paket_ekspedisi : $('.ekspedisi_input:checked').data("service"),
          estimasi        : $('.ekspedisi_input:checked').data("estimasi"),
          metode_pembayaran : $('#jenis_pembayaran option:selected').val(),
          ongkir          : $('.ekspedisi_input:checked').val(),
          total           : $("#total_transaksi").val(),
        }
        console.log(data);
        send_ajax( "<?php echo base_url('Frontend/save_checkout') ?>",data ).then( function(data){
          var dt = JSON.parse(data);
              window.location = "<?php echo base_url('Frontend/confirmation/') ?>"+dt;
        });
    });

    $("#ekspedisi").on("change", function(e){
      if( $("#city").val()  === ""){
          alert("pilih kota dan Provinsi");
          $(".opsi-ekspedisi").html("");
        }else{
          send = $(this).val();
          destination = $("#city").val();

          send_ajax( "<?php echo base_url('Api/get_cost') ?>",{destination:destination, ekspedisi:send} ).then( function(data){
              $(".opsi-ekspedisi").html(data);
          });
        }
        
    });
    $("#jenis_pembayaran").on("change",function(){
      var rek=$(this).find(':selected').data('rek');
      var an=$(this).find(':selected').data('an');
      $('.rk').text("Kirimkan ke rekening "+rek+" (a/n : "+an+")");
    });

    $(".qty").bind('keyup mouseup', function () {
        var id = $(this).data('rowid');
        var value = $(this).val();

        update_cart(id, value);
    });

    $(document.body).on("change",".ekspedisi_input", function(e){
      
      get_total();
    })
  });
  function get_total()
  {
    var ongkir = $('.ekspedisi_input:checked');
    var ekspedisi_input = "";
    if(typeof(ongkir.val()) != "undefined" && ongkir.val() !== null) {
      ekspedisi_input = ongkir.val();
    }

    // alert(ekspedisi_input);
    send_ajax( "<?php echo base_url('frontend/get_total') ?>",{ekspedisi:ekspedisi_input} ).then( function(data){
      $('.total').html(data);
    });
  };
  function update_cart(id, qty)
  {
    send_ajax( "<?php echo base_url('frontend/update_cart') ?>",{id:id, qty:qty} ).then( function(data){
      get_total();
    });
  }
  function set_province()
  {
    send_ajax( "<?php echo base_url('Api/get_province') ?>",{} ).then( function(data){

            $("#province").html(data);
            // console.log(data);
    });
  }
</script>
</body>
</html>