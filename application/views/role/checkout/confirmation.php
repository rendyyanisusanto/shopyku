
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title><?php echo $profil_website['nama_website'] ?></title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/Ionicons/css/ionicons.min.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/bower_components')?>/jvectormap/jquery-jvectormap.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/dist')?>/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url('include/template/adminlte/dist')?>/css/skins/_all-skins.min.css">
  <link rel="stylesheet" href="<?php echo base_url('include/core/core.css')?>">
  <link rel="stylesheet" href="<?php echo base_url('include/template/toastr/toastr.css')?>">
  <link rel="icon" href="<?php echo base_url('include/setting_website/'.$profil_website['icon'])?>">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  
  <script src="<?php echo base_url('include/template/adminlte/bower_components')?>/jquery/dist/jquery.min.js"></script>
  
<script src="<?php echo base_url('include/core/core.js')?>"></script>
  
</head>
<body class="hold-transition skin-blue layout-top-nav">
  <div class="wrapper">
      <div class="content-wrapper">
        <div class="container">
          <div class="row">

            <section class="content">
              <div class="row">
                <center><b><h2>Terimakasih Telah Melakukan Pemesanan</h2></b></center>
                <hr>
                <br>
              </div>
              <div class="row">
                <section class="invoice">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <i class="fa fa-globe"></i> <?php echo $profil_website['nama_website'] ?> 
            <small class="pull-right">Tanggal Transaksi: <?php echo date_format(date_create($data_get['transaksi']['create_at']),'d-m-Y') ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong><?php echo $profil_website['pemilik'] ?> .</strong><br>
            <?php echo $profil_website['alamat'] ?><br>
            <?php echo $data_get['city']['value_add'] ?>, <?php echo $data_get['province']['value_add'] ?> <?php echo $data_get['postal_code']['value_add'] ?><br>
            Phone: (+62)<?php echo $profil_website['no_hp'] ?><br>
            Email: <?php echo $profil_website['email'] ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php echo $data_get['transaksi']['nama'] ?></strong><br>
            <?php echo $data_get['transaksi']['alamat'] ?>
            <?php echo $data_get['transaksi']['kota'] ?>, <?php echo $data_get['transaksi']['provinsi'] ?> <?php echo $data_get['transaksi']['kode_pos'] ?><br>
            Phone: <?php echo $data_get['transaksi']['no_hp'] ?><br>
            Email: <?php echo $data_get['transaksi']['email'] ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <b>Invoice #<?php echo $data_get['transaksi']['kode_transaksi'] ?></b><br>
          <br>
          <b>Order ID:</b> 4F3S8J<br>
          <b>Batas Pembayaran:</b> <?php echo date('d-m-Y', strtotime('+6 days', strtotime($data_get['transaksi']['create_at']))); ?><br>
          <b>Transfer Ke:</b> <?php echo $data_get['transaksi']['metode_pembayaran'] ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
                <th>Produk</th>
                <th>Berat</th>
                <th>Harga</th>
                <th width="20%">QTY</th>
            </tr>
            </thead>
            <tbody>
              <?php foreach ($data_get['barang_transaksi'] as $key => $value): ?>
                <tr>
                  <td style="vertical-align: middle;"><?php echo $value['nama_barang'] ?></td>
                  <td style="vertical-align: middle;"><?php echo $value['berat_barang'] ?> Gram</td>
                  <td style="vertical-align: middle;">Rp. <?php echo number_format($value['harga_barang'],0,',','.') ?></td>
                  <td style="vertical-align: middle;"><?php echo $value['qty'] ?></td>
                </tr>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Metode Pembayaran:</p>
          
          <b><?php echo $data_get['transaksi']['metode_pembayaran'] ?></b>

          <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">
            Kirimkan pembayaran ke Rekening <?php echo $data_get['jenis_pembayaran']['no_rek'] ?> atas nama <?php echo $data_get['jenis_pembayaran']['atas_nama'] ?>. Setelah selesai pembayaran mohon konfirmasi ke <b>No HP / WA (+62)<?php echo $profil_website['no_hp'] ?></b>.
          </p>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">
          <p class="lead">Batas Pembayaran <?php echo date('d-m-Y', strtotime('+6 days', strtotime($data_get['transaksi']['create_at']))); ?></p>

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td>Rp. <?php echo number_format($data_get['transaksi']['total'],0,',','.') ?></td>
              </tr>
              
              <tr>
                <th>Ongkir:</th>
                <td>Rp. <?php echo number_format($data_get['transaksi']['ongkir'],0,',','.') ?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td>Rp. <?php echo number_format(($data_get['transaksi']['total']+$data_get['transaksi']['ongkir']),0,',','.') ?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a onclick="window.print();return false;" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <a href="https://api.whatsapp.com/send?phone=62<?php echo $profil_website['no_hp']?>&text=<?php echo 'Halo Admin, saya ingin mengkonfirmasi pembelian dengan kode transaksi '.$data_get['transaksi']['kode_transaksi']; ?>" class="btn btn-success pull-right"><i class="fa fa-credit-card"></i> Submit Payment
          </a>
          
        </div>
      </div>
    </section>
              </div>
            </section>
          </div>
        </div>
      </div>
  </div>
<!-- /.register-box -->
</body>

<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/adminlte.min.js"></script>
<!--  -->
<script src="<?php echo base_url('include/template/adminlte/bower_components')?>/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- ChartJS -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/pages/dashboard2.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url('include/template/adminlte/dist')?>/js/demo.js"></script>

<script src="<?php echo base_url('include/template/toastr/toastr.min.js')?>"></script>

<script src="<?php echo base_url('include/core/core_shop.js')?>"></script>

</body>
</html>