<ul class="list-group">
    <li class="list-group-item "><span class="pull-right">Rp. <?php echo number_format($total,0,',','.') ?></span> Total Belanja</li>
    <li class="list-group-item "><span class="pull-right">Rp. <?php echo number_format($ongkir,0,',','.') ?></span> Ongkir</li>
    <li class="list-group-item list-group-item-danger"> <b><span class="pull-right">Rp. <?php echo number_format($grand_total,0,',','.') ?></span> Total</b></li>
    <input type="hidden"  id="total_transaksi" value="<?php echo $grand_total ?>" name="">                    
</ul>