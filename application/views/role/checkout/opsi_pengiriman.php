<div class="form-group">
  <label for="inputEmail3" class="col-sm-2 control-label">Pilih Ekspedisi</label>
   
  <div class="col-sm-10">
          <?php foreach ($pengiriman->rajaongkir->results[0]->costs as $key => $value): ?>
            <div class="alert alert-info alert-dismissible">
                <h4><input type="radio" class="ekspedisi_input" value="<?php echo $value->cost[0]->value ?>" data-service="<?php echo $value->service?>" data-estimasi="<?php echo $value->cost[0]->etd ?>" name="ekspedisi"> <?php echo $value->service?>   - (Rp. <?php echo number_format($value->cost[0]->value,0,',','.') ?>) </h4>
                <?php echo $value->description?> - <b>Estimasi <?php echo $value->cost[0]->etd ?> (Hari)</b>
            </div>
              
          <?php endforeach ?>
  </div>
</div>