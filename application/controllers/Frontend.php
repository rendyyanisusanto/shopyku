<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends MY_Frontend {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('MY_Model','mod');
    }

    public function index()
    {
        
        $data   =  $this->data_all();
        // $this->cart->destroy();
        $data['total_items']    = $this->cart->total_items();
        $data['total']          = $this->cart->total();
        $data['testimoni']      =       $this->my_where('testimoni',[])->result();
        $data['produk']         =       $this->my_where('barang',[])->result();
        $data['service']        =       $this->my_where('service',[])->result();
        $data['keunggulan']     =       $this->my_where('keunggulan',[])->result();
        $data['visi']           =       $this->my_where('visi',[]);
        $data['misi']           =       $this->my_where('misi',[]);
        $special_event_query    =       $this->my_query('SELECT * FROM `special_event` where sampai_tanggal  >= now() order by id_special_event DESC limit 1 ') ;
        $data['count_special_event'] = $special_event_query-> num_rows();
        $data['special_event']       = $special_event_query->row();         
    	$this->load->view('role/frontend/'.$data['themes']['themes'].'/index',$data);
    }

    function about_us()
    {
        $data   =  $this->data_all();
        $this->load->view('role/frontend/'.$data['themes']['themes'].'/about_us',$data);
    }
    
    function checkout()
    {
        $data                       =   $this->data_all();
        $data['items']              =   $this->cart->contents();
        $data['province_id']        =   $this->my_where('additional_setting',["key_add" => "province_id"])->row_array();
        $data['jenis_pengiriman']   =   $this->my_where('additional_setting',["key_add" => "jenis_pengiriman"])->row_array();
        $data['jenis_pembayaran']   =   $this->my_where('additional_setting',["key_add" => "jenis_pembayaran"])->row_array();
        $this->load->view('role/checkout/checkout_1',$data);
    }
 
    function katalog()
    {
        $data   =  $this->data_all();
        $this->load->view('role/frontend/'.$data['themes']['themes'].'/katalog',$data);
    }
    
    function kontak()
    {
        $data   =  $this->data_all();
        $this->load->view('role/frontend/'.$data['themes']['themes'].'/kontak',$data);
    }
    
    function blog()
    {
        $data   =  $this->data_all();
        $this->load->view('role/frontend/'.$data['themes']['themes'].'/blog',$data);
    }

    function kegiatan()
    {

        $data   =  $this->data_all();
        $data['kegiatan']     =       $this->my_where('kegiatan',[])->result();
        $this->load->view('role/frontend/'.$data['themes']['themes'].'/kegiatan',$data);
    }

    

    function subscriber()
    {
        $this->db->insert('subscriber',['email'=>$_POST['email']]);
        echo "a";
    }

    function preview()
    {
         $data   =  $this->data_all();
        $data['produk_get']         =       $this->my_where('barang',['id_barang'=>$_POST['id']])->row();
        $data['harga_produk']       =       'Rp.'.number_format($data['produk_get']->harga_barang,0,'.','.');
        if ($data['produk_get']->diskon>0) {
            $total=$data['produk_get']->diskon;
            if ($data['produk_get']->diskon_type=="persen") {
                $total  =   ($data['produk_get']->harga_barang * $data['produk_get']->diskon / 100);
            }        
            $data['harga_produk'] = '<del class="mr-2">'.'Rp.'.number_format($data['produk_get']->harga_barang,0,'.','.').'</del> '.'Rp.'.number_format(($data['produk_get']->harga_barang-$total),0,'.','.');
        }
        $this->load->view('role/frontend/'.$data['themes']['themes'].'/content_ajax',$data);
    }

    function get_product()
    {
        $data   =  $this->data_all();

        $page = $_POST['page'];
        $halaman=$_POST['limit'];
        $mulai = ($page>1) ? ($page * $halaman) - $halaman : 0;
        
        $db=$this->db->get('barang',$halaman,$mulai)->result();
        $total=$this->db->get('barang')->num_rows();

        $pages = ceil($total/$halaman); 
        $data['pagination']='';
        for ($i=1; $i <=$pages ; $i++) { 
            if ($i==$page) {
                # code...
                $data['pagination'].='<span class="page active">'.$i.'</span>';
            }else{
                $data['pagination'].='<a href="#products-section" onclick="get_katalog(start='.$i.',end='.$pages.',limit=6);" class="page">'.$i.'</a>';
            }
            

        }
        $data['produk']=$db;
        $this->load->view('role/frontend/'.$data['themes']['themes'].'/product_ajax',$data);
    }
    function save_email()
    {
        $data = [
            'first_name'    =>      $_POST['first_name'],
            'last_name'     =>      $_POST['last_name'],
            'email'         =>      $_POST['email'],
            'subject'       =>      $_POST['subject'],
            'message'       =>      $_POST['message'],
            'is_read'       =>      0
        ];
        if ($this->db->insert('email_user',$data)) {
            echo "success";
        }

    }

    // chekout

    function add_cart()
    {
        $data = [
            'id'      => $_POST['id_barang'],
            'qty'     => 1,
            'price'   => $_POST['harga_barang'],
            'name'    => $_POST['nama_barang'],
            'weight'  => $_POST['weight'],
            'img'     => $_POST['img']
        ];
        $this->cart->insert($data);
    }

    function update_cart()
    {
        $data   =   [
            'rowid' =>  $_POST['id'],
            'qty'   =>  $_POST['qty']
        ];

        $this->cart->update($data);
    }

    function get_count_cart()
    {
        $total_items    = $this->cart->total_items();
        $total          = $this->cart->total();

        echo "Rp. ".number_format($total,0,',','.')."(".$total_items." item(s))";
    }

    function get_total()
    {
        $data['total']          =   $this->cart->total();
        $data['ongkir']         =   (!empty($_POST['ekspedisi']))?$_POST['ekspedisi']:0;
        $data['grand_total']    =   $data['total']+$data['ongkir'];
        // echo "<center><h3>Rp. ".number_format($total,0,',','.').'</h3></center>';
        $this->load->view('role/checkout/total_belanja',$data);
    }

    function save_checkout()
    {
        $data = [
            'kode_transaksi'        =>  md5(rand(1,9999999)),
            'nama'                  =>  $_POST['nama'],
            'no_hp'                 =>  $_POST['no_hp'],
            'email'                 =>  $_POST['email'],
            'provinsi'              =>  $_POST['provinsi'],
            'kota'                  =>  $_POST['kota'],
            'alamat'                =>  $_POST['alamat'],
            'kode_pos'              =>  $_POST['kode_pos'],
            'ekspedisi'             =>  $_POST['ekspedisi'],
            'paket_ekspedisi'       =>  $_POST['paket_ekspedisi'],
            'estimasi'              =>  $_POST['estimasi'],
            'metode_pembayaran'     =>  $_POST['metode_pembayaran'],
            'ongkir'                =>  $_POST['ongkir'],
            'total'                 =>  $_POST['total'],
            'status_transaksi'      =>  0
        ];
        $this->db->insert("transaksi", $data);
        $id = $this->db->get_where("transaksi", $data)->row_array();

        foreach ($this->cart->contents() as $key => $value) {
            $databarang = [
                'idbarang_fk'       =>  $value['id'],
                'idtransaksi_fk'    =>  $id['id_transaksi'],
                'qty'               =>  $value['qty']
            ];

            $this->db->insert('barang_transaksi', $databarang);
        }

        echo json_encode($id['id_transaksi']);
    }
    function confirmation($id)
    {
        $idtrans = $this->db->get_where("transaksi", ["id_transaksi"=>$id])->row_array();
        $barang_transaksi = $this->db->get_where("v_barang_transaksi", ["idtransaksi_fk"=>$id])->result_array();
        $data                       =   $this->data_all();
        $data['data_get']['transaksi']  =   $idtrans;
        $data['data_get']['barang_transaksi']  =   $barang_transaksi;

            $data['data_get']['city']               =   $this->my_where('additional_setting',["key_add" => "province"])->row_array();
            $data['data_get']['province']           =   $this->my_where('additional_setting',["key_add" => "city"])->row_array();
            $data['data_get']['postal_code']        =   $this->my_where('additional_setting',["key_add" => "postal_code"])->row_array();
            $data['data_get']['jenis_pembayaran']   =   [];   
            $pembayaran =   json_decode($this->my_where('additional_setting',["key_add" => "jenis_pembayaran"])->row_array()['value_add']);
            
            foreach ($pembayaran as $key => $value) {
                if ($value->text == $data['data_get']['transaksi']['metode_pembayaran']) {
                    $data['data_get']['jenis_pembayaran'] = [
                        'text'      =>  $value->text,
                        'no_rek'    =>  $value->no_rek,
                        'atas_nama' =>  $value->atas_nama
                    ];
                }
            }
        $this->load->view('role/checkout/confirmation',$data);
    }
}
