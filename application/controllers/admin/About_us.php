<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About_us extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/

	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$data['profil']		=	$this->my_where('profil_website',[])->row_array();
		$this->my_view(['role/admin/page/about_us/index','role/admin/page/about_us/js_about_us'],$data);
	}

	/*
		ADD DATA
	*/


	function update_data()
	{

		$data=[];
		$data=[
				'tagline'					=>	$_POST['tagline'],
				'about_us'					=>	$_POST['about_us'],
			];

		if (!empty($_FILES['img_about_us']['name'])) {
			
			$config2['upload_path']="./include/setting_website/img_about_us";
	        $config2['allowed_types']='gif|jpg|png|jpeg';
	        $config2['encrypt_name'] = TRUE;

	        $this->load->library('upload',$config2);
	        if($this->upload->do_upload("img_about_us")){
	            $data2 = array('upload_data' => $this->upload->data());
	            $data['img_about_us'] = $data2['upload_data']['file_name'];
	        }
        }

        

				
		if ($this->my_update('profil_website',$data,[])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}


	}

	/*
		DELETE DATA
	*/

}