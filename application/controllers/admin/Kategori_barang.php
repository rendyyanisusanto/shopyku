<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class kategori_barang extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/


	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/kategori_barang/index','role/admin/page/kategori_barang/js_kategori_barang'],$data);
	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/kategori_barang/add_data','role/admin/page/kategori_barang/js_add_page'],$data);
	}

	function edit_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('kategori_barang',['id_kategori_barang'=>$value])->row_array();
				$data_edit[]	=	[
										'id_kategori_barang'							=>	$data_set['id_kategori_barang'],
										'kategori'								=>	$data_set['kategori'],
				];
			}

			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/kategori_barang/edit_data','role/admin/page/kategori_barang/js_edit_page'],$data);
		} else {
			$this->get_data();
		}
		
	}
	
	/*
		ADD DATA
	*/


	public function simpan_data()
	{
            $data=[
			'kategori'					=>	$_POST['kategori'],
			];
				if ($this->save_data('kategori_barang',$data)) {
					$this->get_data();
				}	else 	{
					echo "error";
				}
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		$data=[
			'kategori'		=>	$_POST['kategori'],		
			];
		if ($this->my_update('kategori_barang',$data,['id_kategori_barang'=>$_POST['id_kategori_barang']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}

	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete('kategori_barang',['id_kategori_barang'=>$value]);
		}
	}

	/*
		PRINT DATA
	*/

	function cetak_data()
	{
		$this->my_delete_file(FCPATH.'/include/pdf_temp');

		$data=[];
		$where_send		=	[];

		if ($_POST['data_yg_dicetak']	==	'manual') {
			
			if (!empty($_POST['nama_kategori_barang'])) {
				$where_send['nama_kategori_barang']	=	$_POST['nama_kategori_barang'];
			}

			if (!empty($_POST['is_active'])) {
				$where_send['is_active']	=	$_POST['is_active'];
			}

			
		} else if($_POST['data_yg_dicetak']	==	'pilih')
		{
			$data_selected = explode(',', $_POST['input_selected']);
			foreach ($data_selected as $key => $value) {
				$this->db->or_where('id_kategori_barang', $value);
			}
		}

		$data_set = $this->my_where('kategori_barang',$where_send);
		
		$url	=	($_POST['laporan']	==	'data')	?	'role/admin/page/kategori_barang/cetak_data'	:	'role/admin/page/kategori_barang/cetak_kartu';
		
	    if ($_POST['tipe_laporan'] == 'pdf') {

	    	$param	=	[
                'url'			=>	$url,
                'customPaper'	=>	array(0,0,381.89,595.28),
                'data_value'	=>	[
                	"data"		=>	$data_set->result_array()
                ],
                'name'			=>	md5(rand(0,9999999)),
                'pos' 			=> 'landscape'
            ];

            $this->my_pdf($param);

	    }
	    
	    else if($_POST['tipe_laporan'] == 'excel')

	    {
	    	
            $param  =   [
                'filename'			=>		'Jadwal Kegiatan Sekolah',
                'data_obj'			=>		$data_set->result(),
                'header_table'		=>		['Nama', 'tanggal mulai', 'tanggal selesai', 'keterangan'],
                'print_field'		=>		['nama_kategori_barang', 'tanggal_mulai', 'tanggal_selesai', 'keterangan']
            ];

            $this->my_export_excel($param);
        
	    }

	}

	/*
		MANIPULATE DATA
	*/

	public function datatable()
	{
		$data_send = [
			'table'				=>	'kategori_barang',
			'column_order'		=>	['kategori','create_at'],
			'column_search'		=>	['kategori','create_at'],
			'order'				=>	['id_kategori_barang'	=>	'DESC'],
		];

		$_POST['frm']	=	$data_send;
		
		if (!empty($_POST['kategori'])) {
			$this->db->where('kategori',$_POST['kategori']);
		}

		if (!empty($_POST['create_at'])) {
			$this->db->where('create_at',$_POST['create_at']);
		}
		

		$list = $this->mod_datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" name="get-check" value="'.$field->id_kategori_barang.'"></input>';
            $row[] = $field->kategori;
            $row[] = $field->create_at;
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	
	function filter()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/kategori_barang/filter'],$data);
	}

}