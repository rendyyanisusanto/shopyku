<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class testimoni extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/


	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/testimoni/index','role/admin/page/testimoni/js_testimoni'],$data);
	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/testimoni/add_data','role/admin/page/testimoni/js_add_page'],$data);
	}

	function edit_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('testimoni',['id_testimoni'=>$value])->row_array();
				$data_edit[]	=	[
										'id_testimoni'							=>	$data_set['id_testimoni'],
										'nama'									=>	$data_set['nama'],
										'foto'									=>	$data_set['foto'],
										'testimoni'								=>	$data_set['testimoni'],
				];
			}

			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/testimoni/edit_data','role/admin/page/testimoni/js_edit_page'],$data);
		} else {
			$this->get_data();
		}
		
	}
	
	/*
		ADD DATA
	*/


	public function simpan_data()
	{

		$config2['upload_path']="./include/testimoni";
        $config2['allowed_types']='gif|jpg|png';
        $config2['encrypt_name'] = TRUE;

        $this->load->library('upload',$config2);
        if($this->upload->do_upload("foto")){
            $data2 = array('upload_data' => $this->upload->data());
            $data=[
			'nama'					=>	$_POST['nama'],
			'testimoni'				=>	$_POST['testimoni'],
			'foto'					=>	$data2['upload_data']['file_name'],
			];

			
				if ($this->save_data('testimoni',$data)) {
					$this->get_data();
				}	else 	{
					echo "error";
				}
				
        }

		
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		// foreach ($_POST['data'] as $key => $value) {
		// 	if ($this->my_db_count('testimoni',['id_testimoni'=>$value['id_testimoni']])>0) {
		// 		$this->my_update('testimoni'
		// 			,[
		// 				'nama_keterangan'				=>	$value['nama_keterangan'],
		// 				'tanggal_mulai'					=>	$value['tanggal_mulai'],
		// 				'tanggal_selesai'				=>	$value['tanggal_selesai'],
		// 			]
		// 			,['id_testimoni'=>$value['id_testimoni']]);
		// 	}
		// }
		

		$data=[];
		$data=[
			'nama'					=>	$_POST['nama'],
			'testimoni'				=>	$_POST['testimoni'],
			
			];
		if (!empty($_FILES['foto']['name'])) {
			
			$config2['upload_path']="./include/testimoni";
	        $config2['allowed_types']='gif|jpg|png';
	        $config2['encrypt_name'] = TRUE;

	        $this->load->library('upload',$config2);
	        if($this->upload->do_upload("foto")){
	            $data2 = array('upload_data' => $this->upload->data());
	            $data['foto'] = $data2['upload_data']['file_name'];
				
	        }
        }

        

				
		if ($this->my_update('testimoni',$data,['id_testimoni'=>$_POST['id_testimoni']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}

	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete('testimoni',['id_testimoni'=>$value]);
		}
	}

	/*
		PRINT DATA
	*/

	function cetak_data()
	{
		$this->my_delete_file(FCPATH.'/include/pdf_temp');

		$data=[];
		$where_send		=	[];

		if ($_POST['data_yg_dicetak']	==	'manual') {
			
			if (!empty($_POST['nama_testimoni'])) {
				$where_send['nama_testimoni']	=	$_POST['nama_testimoni'];
			}

			if (!empty($_POST['is_active'])) {
				$where_send['is_active']	=	$_POST['is_active'];
			}

			
		} else if($_POST['data_yg_dicetak']	==	'pilih')
		{
			$data_selected = explode(',', $_POST['input_selected']);
			foreach ($data_selected as $key => $value) {
				$this->db->or_where('id_testimoni', $value);
			}
		}

		$data_set = $this->my_where('testimoni',$where_send);
		
		$url	=	($_POST['laporan']	==	'data')	?	'role/admin/page/testimoni/cetak_data'	:	'role/admin/page/testimoni/cetak_kartu';
		
	    if ($_POST['tipe_laporan'] == 'pdf') {

	    	$param	=	[
                'url'			=>	$url,
                'customPaper'	=>	array(0,0,381.89,595.28),
                'data_value'	=>	[
                	"data"		=>	$data_set->result_array()
                ],
                'name'			=>	md5(rand(0,9999999)),
                'pos' 			=> 'landscape'
            ];

            $this->my_pdf($param);

	    }
	    
	    else if($_POST['tipe_laporan'] == 'excel')

	    {
	    	
            $param  =   [
                'filename'			=>		'Jadwal Kegiatan Sekolah',
                'data_obj'			=>		$data_set->result(),
                'header_table'		=>		['Nama', 'tanggal mulai', 'tanggal selesai', 'keterangan'],
                'print_field'		=>		['nama_testimoni', 'tanggal_mulai', 'tanggal_selesai', 'keterangan']
            ];

            $this->my_export_excel($param);
        
	    }

	}

	/*
		MANIPULATE DATA
	*/

	public function datatable()
	{
		$data_send = [
			'table'				=>	'testimoni',
			'column_order'		=>	['nama','testimoni','create_at'],
			'column_search'		=>	['nama','testimoni','create_at'],
			'order'				=>	['id_testimoni'	=>	'DESC'],
		];

		$_POST['frm']	=	$data_send;
		if (!empty($_POST['nama'])) {
			$this->db->where('nama',$_POST['nama']);
		}
		
		if (!empty($_POST['testimoni'])) {
			$this->db->where('testimoni',$_POST['testimoni']);
		}

		if (!empty($_POST['create_at'])) {
			$this->db->where('create_at',$_POST['create_at']);
		}
		

		$list = $this->mod_datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" name="get-check" value="'.$field->id_testimoni.'"></input>';
            $row[] = $field->nama;
            $row[] = $field->testimoni;
            $row[] = $field->create_at;
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	
	function filter()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/testimoni/filter'],$data);
	}

}