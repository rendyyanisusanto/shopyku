<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class subscriber extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/


	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/subscriber/index','role/admin/page/subscriber/js_subscriber'],$data);
	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/subscriber/add_data','role/admin/page/subscriber/js_add_page'],$data);
	}

	public function edit_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('subscriber',['id_subscriber'=>$value])->row_array();
				$data_edit[]	=	[
										'id_subscriber'							=>	$data_set['id_subscriber'],
										'email'									=>	$data_set['email'],
										
				];
			}

			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/subscriber/edit_data','role/admin/page/subscriber/js_edit_page'],$data);
		} else {
			$this->get_data();
		}
		
	}

	/*
		ADD DATA
	*/


	public function simpan_data()
	{

		
            $data=[
			'email'					=>	$_POST['email'],
			];

			
				if ($this->save_data('subscriber',$data)) {
					$this->get_data();
				}	else 	{
					echo "error";
				}
				
      

		
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		
		$data=[];
		$data=[
				'email'		=>	$_POST['email'],
		];

		if ($this->my_update('subscriber',$data,['id_subscriber'=>$_POST['id_subscriber']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}

	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete('subscriber',['id_subscriber'=>$value]);
		}
	}

	
	/*
		MANIPULATE DATA
	*/

	public function datatable()
	{
		$data_send = [
			'table'				=>	'subscriber',
			'column_order'		=>	['email','create_at'],
			'column_search'		=>	['email','create_at'],
			'order'				=>	['id_subscriber'	=>	'DESC'],
		];

		$_POST['frm']	=	$data_send;
		if (!empty($_POST['email'])) {
			$this->db->where('email',$_POST['email']);
		}
		
		$list = $this->mod_datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" class="checkbox" name="get-check" value="'.$field->id_subscriber.'"></input>';
            $row[] = $field->email;     
            $row[] = date_format(date_create($field->create_at),'d-m-Y');
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	


}