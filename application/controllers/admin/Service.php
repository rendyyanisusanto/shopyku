<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class service extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/


	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/service/index','role/admin/page/service/js_service'],$data);
	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/service/add_data','role/admin/page/service/js_add_page'],$data);
	}

	public function edit_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('service',['id_service'=>$value])->row_array();
				$data_edit[]	=	[
										'id_service'							=>	$data_set['id_service'],
										'nama_service'							=>	$data_set['nama_service'],
										'stok'									=>	$data_set['stok'],
										'harga_service'							=>	$data_set['harga_service'],
										'deskripsi'								=>	$data_set['deskripsi'],
										'foto'									=>	$data_set['foto'],
				];
			}

			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/service/edit_data','role/admin/page/service/js_edit_page'],$data);
		} else {
			$this->get_data();
		}
		
	}

	function cetak_page()
	{
		$data['sum_selected']=0;
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('service',['id_service'=>$value])->row_array();
				$data_edit[]	=	[
										'id_service'							=>	$data_set['id_service'],
										'nama_service'						=>	$data_set['nama_service'],
										'stok'									=>	$data_set['stok'],
										'harga_service'						=>	$data_set['harga_service'],
				];
			}

			$data['data_edit']			=	$data_edit;
			$data['sum_selected']		=	count($_POST['send_data']);
			$data['input_selected']		=	implode(',', $_POST['send_data']);
			
		}

		$this->my_view(['role/admin/page/service/cetak_page','role/admin/page/service/js_cetak'],$data);
		
	}
	
	/*
		ADD DATA
	*/


	public function simpan_data()
	{

		
            $data=[
			'judul'					=>	$_POST['judul'],
			'text'					=>	$_POST['txt'],
			'icon'					=>	$_POST['iconion'],
			];

			
				if ($this->save_data('service',$data)) {
					$this->get_data();
				}	else 	{
					echo "error";
				}
				
      

		
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		// foreach ($_POST['data'] as $key => $value) {
		// 	if ($this->my_db_count('service',['id_service'=>$value['id_service']])>0) {
		// 		$this->my_update('service'
		// 			,[
		// 				'nama_keterangan'				=>	$value['nama_keterangan'],
		// 				'tanggal_mulai'					=>	$value['tanggal_mulai'],
		// 				'tanggal_selesai'				=>	$value['tanggal_selesai'],
		// 			]
		// 			,['id_service'=>$value['id_service']]);
		// 	}
		// }
		$data=[];
		$data=[
				'nama_service'		=>	$_POST['nama_service'],
				'harga_service'		=>	$_POST['harga_service'],
				'stok'				=>	$_POST['stok'],
				'deskripsi'			=>	$_POST['deskripsi'],
				];
		if (!empty($_FILES['foto_produk']['name'])) {
			
			$this->my_delete_file(FCPATH.'/include/foto_produk/'.$_POST['foto_lama']);
			$config2['upload_path']="./include/foto_produk";
	        $config2['allowed_types']='gif|jpg|png|jpeg';
	        $config2['encrypt_name'] = TRUE;

	        $this->load->library('upload',$config2);
	        if($this->upload->do_upload("foto_produk")){
	            $data2 = array('upload_data' => $this->upload->data());
	            $data['foto'] = $data2['upload_data']['file_name'];
				
	        }
        }

        

				
		if ($this->my_update('service',$data,['id_service'=>$_POST['id_service']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}

	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete('service',['id_service'=>$value]);
		}
	}

	/*
		PRINT DATA
	*/

	function cetak_data()
	{
		$this->my_delete_file(FCPATH.'/include/pdf_temp');

		$data=[];
		$where_send		=	[];

		if ($_POST['data_yg_dicetak']	==	'manual') {
			
			if (!empty($_POST['nama_service'])) {
				$where_send['nama_service']	=	$_POST['nama_service'];
			}

			if (!empty($_POST['is_active'])) {
				$where_send['is_active']	=	$_POST['is_active'];
			}

			
		} else if($_POST['data_yg_dicetak']	==	'pilih')
		{
			$data_selected = explode(',', $_POST['input_selected']);
			foreach ($data_selected as $key => $value) {
				$this->db->or_where('id_service', $value);
			}
		}

		$data_set = $this->my_where('service',$where_send);
		
		$url	=	($_POST['laporan']	==	'data')	?	'role/admin/page/service/cetak_data'	:	'role/admin/page/service/cetak_kartu';
		
	    if ($_POST['tipe_laporan'] == 'pdf') {

	    	$param	=	[
                'url'			=>	$url,
                'customPaper'	=>	array(0,0,381.89,595.28),
                'data_value'	=>	[
                	"data"		=>	$data_set->result_array()
                ],
                'name'			=>	md5(rand(0,9999999)),
                'pos' 			=> 'landscape'
            ];

            $this->my_pdf($param);

	    }
	    
	    else if($_POST['tipe_laporan'] == 'excel')

	    {
	    	
            $param  =   [
                'filename'			=>		'Jadwal Kegiatan Sekolah',
                'data_obj'			=>		$data_set->result(),
                'header_table'		=>		['Nama', 'tanggal mulai', 'tanggal selesai', 'keterangan'],
                'print_field'		=>		['nama_service', 'tanggal_mulai', 'tanggal_selesai', 'keterangan']
            ];

            $this->my_export_excel($param);
        
	    }

	}

	/*
		MANIPULATE DATA
	*/

	public function datatable()
	{
		$data_send = [
			'table'				=>	'service',
			'column_order'		=>	['judul','text','icon'],
			'column_search'		=>	['judul','text','icon'],
			'order'				=>	['id_service'	=>	'DESC'],
		];

		$_POST['frm']	=	$data_send;
		if (!empty($_POST['judul'])) {
			$this->db->where('judul',$_POST['judul']);
		}
		
		if (!empty($_POST['text'])) {
			$this->db->where('text',$_POST['text']);
		}

		if (!empty($_POST['icon'])) {
			$this->db->where('icon',$_POST['icon']);
		}
		

		$list = $this->mod_datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" name="get-check" value="'.$field->id_service.'"></input>';
            $row[] = $field->judul;
            $row[] = $field->text;
            $row[] = $field->icon;
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	
	function filter()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/service/filter'],$data);
	}

}