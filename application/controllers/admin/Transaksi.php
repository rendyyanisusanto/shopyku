<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class transaksi extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/


	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/transaksi/index','role/admin/page/transaksi/js_transaksi'],$data);
	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/transaksi/add_data','role/admin/page/transaksi/js_add_page'],$data);
	}

	public function edit_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('transaksi',['id_transaksi'=>$value])->row_array();
				$data_edit[]	=	[
										'id_transaksi'							=>	$data_set['id_transaksi'],
										'nama_transaksi'							=>	$data_set['nama_transaksi'],
										'stok'									=>	$data_set['stok'],
										'harga_transaksi'							=>	$data_set['harga_transaksi'],
										'deskripsi'								=>	$data_set['deskripsi'],
										'foto'									=>	$data_set['foto'],
				];
			}

			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/transaksi/edit_data','role/admin/page/transaksi/js_edit_page'],$data);
		} else {
			$this->get_data();
		}
		
	}

	public function detail_page()
	{
		if (isset($_POST['send_data'])) {


			$data['transaksi']			=	$this->my_where("transaksi", ['id_transaksi' => $_POST['send_data']])->row_array();
			$data['barang_transaksi']	=	$this->my_where("v_barang_transaksi", ['idtransaksi_fk' => $data['transaksi']['id_transaksi']])->result_array();

			
			$this->my_view(['role/admin/page/transaksi/detail_page'],$data);
		} else {
			$this->get_data();
		}
		
	}

	public function ubah_status()
	{
		if (isset($_POST['send_data'])) {


			$data['transaksi']			=	$this->my_where("transaksi", ['id_transaksi' => $_POST['send_data']])->row_array();
			$data['barang_transaksi']	=	$this->my_where("v_barang_transaksi", ['idtransaksi_fk' => $data['transaksi']['id_transaksi']])->result_array();

			
			$this->my_view(['role/admin/page/transaksi/ubah_page','role/admin/page/transaksi/js_ubah_status'],$data);
		} else {
			$this->get_data();
		}
		
	}

	

	public function invoice()
	{
		if (isset($_POST['send_data'])) {
			$data['transaksi']			=	$this->my_where("transaksi", ['id_transaksi' => $_POST['send_data']])->row_array();
			$data['barang_transaksi']	=	$this->my_where("v_barang_transaksi", ['idtransaksi_fk' => $data['transaksi']['id_transaksi']])->result_array();

	        $data['city']       		=   $this->my_where('additional_setting',["key_add" => "province"])->row_array();
	        $data['province']        	=   $this->my_where('additional_setting',["key_add" => "city"])->row_array();
	        $data['postal_code']        =   $this->my_where('additional_setting',["key_add" => "postal_code"])->row_array();
			$data['jenis_pembayaran']	=	[];   
        	$pembayaran =   json_decode($this->my_where('additional_setting',["key_add" => "jenis_pembayaran"])->row_array()['value_add']);
        	
        	foreach ($pembayaran as $key => $value) {
        		if ($value->text == $data['transaksi']['metode_pembayaran']) {
        			$data['jenis_pembayaran'] = [
        				'text'		=>	$value->text,
        				'no_rek'	=>	$value->no_rek,
        				'atas_nama'	=>	$value->atas_nama
        			];
        		}
        	}
			$this->my_view(['role/admin/page/transaksi/invoice'],$data);
		} else {
			$this->get_data();
		}
		
	}

	function cetak_page()
	{
		$data['sum_selected']=0;
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('transaksi',['id_transaksi'=>$value])->row_array();
				$data_edit[]	=	[
										'id_transaksi'							=>	$data_set['id_transaksi'],
										'nama_transaksi'						=>	$data_set['nama_transaksi'],
										'stok'									=>	$data_set['stok'],
										'harga_transaksi'						=>	$data_set['harga_transaksi'],
				];
			}

			$data['data_edit']			=	$data_edit;
			$data['sum_selected']		=	count($_POST['send_data']);
			$data['input_selected']		=	implode(',', $_POST['send_data']);
			
		}

		$this->my_view(['role/admin/page/transaksi/cetak_page','role/admin/page/transaksi/js_cetak'],$data);
		
	}
	
	/*
		ADD DATA
	*/


	public function simpan_data()
	{

		
            $data=[
			'judul'					=>	$_POST['judul'],
			'text'					=>	$_POST['txt'],
			'icon'					=>	$_POST['iconion'],
			];

			
				if ($this->save_data('transaksi',$data)) {
					$this->get_data();
				}	else 	{
					echo "error";
				}
				
      

		
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		// foreach ($_POST['data'] as $key => $value) {
		// 	if ($this->my_db_count('transaksi',['id_transaksi'=>$value['id_transaksi']])>0) {
		// 		$this->my_update('transaksi'
		// 			,[
		// 				'nama_keterangan'				=>	$value['nama_keterangan'],
		// 				'tanggal_mulai'					=>	$value['tanggal_mulai'],
		// 				'tanggal_selesai'				=>	$value['tanggal_selesai'],
		// 			]
		// 			,['id_transaksi'=>$value['id_transaksi']]);
		// 	}
		// }
		$data=[];
		$data=[
				'nama_transaksi'		=>	$_POST['nama_transaksi'],
				'harga_transaksi'		=>	$_POST['harga_transaksi'],
				'stok'				=>	$_POST['stok'],
				'deskripsi'			=>	$_POST['deskripsi'],
				];
		if (!empty($_FILES['foto_produk']['name'])) {
			
			$this->my_delete_file(FCPATH.'/include/foto_produk/'.$_POST['foto_lama']);
			$config2['upload_path']="./include/foto_produk";
	        $config2['allowed_types']='gif|jpg|png|jpeg';
	        $config2['encrypt_name'] = TRUE;

	        $this->load->library('upload',$config2);
	        if($this->upload->do_upload("foto_produk")){
	            $data2 = array('upload_data' => $this->upload->data());
	            $data['foto'] = $data2['upload_data']['file_name'];
				
	        }
        }

        

				
		if ($this->my_update('transaksi',$data,['id_transaksi'=>$_POST['id_transaksi']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}

	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete('transaksi',['id_transaksi'=>$value]);
		}
	}

	/*
		PRINT DATA
	*/

	function cetak_data()
	{
		$this->my_delete_file(FCPATH.'/include/pdf_temp');

		$data=[];
		$where_send		=	[];

		if ($_POST['data_yg_dicetak']	==	'manual') {
			
			if (!empty($_POST['nama_transaksi'])) {
				$where_send['nama_transaksi']	=	$_POST['nama_transaksi'];
			}

			if (!empty($_POST['is_active'])) {
				$where_send['is_active']	=	$_POST['is_active'];
			}

			
		} else if($_POST['data_yg_dicetak']	==	'pilih')
		{
			$data_selected = explode(',', $_POST['input_selected']);
			foreach ($data_selected as $key => $value) {
				$this->db->or_where('id_transaksi', $value);
			}
		}

		$data_set = $this->my_where('transaksi',$where_send);
		
		$url	=	($_POST['laporan']	==	'data')	?	'role/admin/page/transaksi/cetak_data'	:	'role/admin/page/transaksi/cetak_kartu';
		
	    if ($_POST['tipe_laporan'] == 'pdf') {

	    	$param	=	[
                'url'			=>	$url,
                'customPaper'	=>	array(0,0,381.89,595.28),
                'data_value'	=>	[
                	"data"		=>	$data_set->result_array()
                ],
                'name'			=>	md5(rand(0,9999999)),
                'pos' 			=> 'landscape'
            ];

            $this->my_pdf($param);

	    }
	    
	    else if($_POST['tipe_laporan'] == 'excel')

	    {
	    	
            $param  =   [
                'filename'			=>		'Jadwal Kegiatan Sekolah',
                'data_obj'			=>		$data_set->result(),
                'header_table'		=>		['Nama', 'tanggal mulai', 'tanggal selesai', 'keterangan'],
                'print_field'		=>		['nama_transaksi', 'tanggal_mulai', 'tanggal_selesai', 'keterangan']
            ];

            $this->my_export_excel($param);
        
	    }

	}

	/*
		MANIPULATE DATA
	*/

	public function datatable()
	{
		$data_send = [
			'table'				=>	'transaksi',
			'column_order'		=>	['kode_transaksi','nama','no_hp','email','provinsi','kota','alamat','kode_pos','ekspedisi','paket_ekspedisi','estimasi','metode_pembayaran','ongkir','kupon','total','status_transaksi'],
			'column_search'		=>	['kode_transaksi','nama','no_hp','email','provinsi','kota','alamat','kode_pos','ekspedisi','paket_ekspedisi','estimasi','metode_pembayaran','ongkir','kupon','total','status_transaksi'],
			'order'				=>	['id_transaksi'	=>	'DESC'],
		];
		$_POST['frm']	=	$data_send;
		if (!empty($_POST['judul'])) {
			$this->db->where('judul',$_POST['judul']);
		}
		
		if (!empty($_POST['text'])) {
			$this->db->where('text',$_POST['text']);
		}

		if (!empty($_POST['icon'])) {
			$this->db->where('icon',$_POST['icon']);
		}
		

		$list = $this->mod_datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" name="get-check" value="'.$field->id_transaksi.'"></input>';
            $row[] = ($field->status_transaksi == 0) ? "<span class='label label-danger'>Belum Dikonfirmasi</span>":"<span class='label label-success'>Terkonfirmasi</span>" ;
            $row[] = $field->nama;
            $row[] = $field->email;
            $row[] = $field->provinsi.'/'.$field->kota;
            $row[] = $field->alamat;
            $row[] = $field->ekspedisi;
            $row[] = $field->paket_ekspedisi;
            $row[] = 'Rp '.number_format($field->total,0,',','.');
            $data[] = $row;
        }

        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	
	function filter()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/transaksi/filter'],$data);
	}

	function ubah_status_save()
	{
		$data=[];
		$data=[
				'status_transaksi'			=>	$_POST['status'],
				];
		if (!empty($_FILES['foto_bukti']['name'])) {
			
			$config2['upload_path']="./include/foto_bukti";
	        $config2['allowed_types']='gif|jpg|png|jpeg';
	        $config2['encrypt_name'] = TRUE;

	        $this->load->library('upload',$config2);
	        if($this->upload->do_upload("foto_bukti")){
	            $data2 = array('upload_data' => $this->upload->data());
	            $data['foto_bukti'] = $data2['upload_data']['file_name'];
				
	        }
        }
				
		if ($this->my_update('transaksi',$data,['id_transaksi'=>$_POST['id_transaksi']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}
	}
}