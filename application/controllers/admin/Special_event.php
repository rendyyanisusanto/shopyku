<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class special_event extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/


	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/special_event/index','role/admin/page/special_event/js_special_event'],$data);
	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/special_event/add_data','role/admin/page/special_event/js_add_page'],$data);
	}

	public function edit_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('special_event',['id_special_event'=>$value])->row_array();
				$data_edit[]	=	[
										'id_special_event'						=>	$data_set['id_special_event'],
										'nama_special_event'					=>	$data_set['nama_special_event'],
										'stok'									=>	$data_set['stok'],
										'harga_special_event'					=>	$data_set['harga_special_event'],
										'deskripsi'								=>	$data_set['deskripsi'],
										'foto'									=>	$data_set['foto'],
				];
			}

			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/special_event/edit_data','role/admin/page/special_event/js_edit_page'],$data);
		} else {
			$this->get_data();
		}
		
	}

	/*
		ADD DATA
	*/


	public function simpan_data()
	{

		
            $data=[
			'judul'					=>	$_POST['judul'],
			'deskripsi'				=>	$_POST['deskripsi'],
			'sampai_tanggal'		=>	$_POST['sampai_tanggal'],
			];

			
				if ($this->save_data('special_event',$data)) {
					$this->get_data();
				}	else 	{
					echo "error";
				}
				
      

		
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		// foreach ($_POST['data'] as $key => $value) {
		// 	if ($this->my_db_count('special_event',['id_special_event'=>$value['id_special_event']])>0) {
		// 		$this->my_update('special_event'
		// 			,[
		// 				'nama_keterangan'				=>	$value['nama_keterangan'],
		// 				'tanggal_mulai'					=>	$value['tanggal_mulai'],
		// 				'tanggal_selesai'				=>	$value['tanggal_selesai'],
		// 			]
		// 			,['id_special_event'=>$value['id_special_event']]);
		// 	}
		// }
		$data=[];
		 $data=[
			'judul'					=>	$_POST['judul'],
			'deskripsi'				=>	$_POST['deskripsi'],
			'sampai_tanggal'		=>	$_POST['sampai_tanggal'],
			];
		

        

				
		if ($this->my_update('special_event',$data,['id_special_event'=>$_POST['id_special_event']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}

	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete('special_event',['id_special_event'=>$value]);
		}
	}

	/*
		PRINT DATA
	*/

	function cetak_data()
	{
		$this->my_delete_file(FCPATH.'/include/pdf_temp');

		$data=[];
		$where_send		=	[];

		if ($_POST['data_yg_dicetak']	==	'manual') {
			
			if (!empty($_POST['nama_special_event'])) {
				$where_send['nama_special_event']	=	$_POST['nama_special_event'];
			}

			if (!empty($_POST['is_active'])) {
				$where_send['is_active']	=	$_POST['is_active'];
			}

			
		} else if($_POST['data_yg_dicetak']	==	'pilih')
		{
			$data_selected = explode(',', $_POST['input_selected']);
			foreach ($data_selected as $key => $value) {
				$this->db->or_where('id_special_event', $value);
			}
		}

		$data_set = $this->my_where('special_event',$where_send);
		
		$url	=	($_POST['laporan']	==	'data')	?	'role/admin/page/special_event/cetak_data'	:	'role/admin/page/special_event/cetak_kartu';
		
	    if ($_POST['tipe_laporan'] == 'pdf') {

	    	$param	=	[
                'url'			=>	$url,
                'customPaper'	=>	array(0,0,381.89,595.28),
                'data_value'	=>	[
                	"data"		=>	$data_set->result_array()
                ],
                'name'			=>	md5(rand(0,9999999)),
                'pos' 			=> 'landscape'
            ];

            $this->my_pdf($param);

	    }
	    
	    else if($_POST['tipe_laporan'] == 'excel')

	    {
	    	
            $param  =   [
                'filename'			=>		'Jadwal Kegiatan Sekolah',
                'data_obj'			=>		$data_set->result(),
                'header_table'		=>		['Nama', 'tanggal mulai', 'tanggal selesai', 'keterangan'],
                'print_field'		=>		['nama_special_event', 'tanggal_mulai', 'tanggal_selesai', 'keterangan']
            ];

            $this->my_export_excel($param);
        
	    }

	}

	/*
		MANIPULATE DATA
	*/

	public function datatable()
	{
		$data_send = [
			'table'				=>	'special_event',
			'column_order'		=>	['judul','deskripsi','sampai_tanggal'],
			'column_search'		=>	['judul','deskripsi','sampai_tanggal'],
			'order'				=>	['id_special_event'	=>	'DESC'],
		];

		$_POST['frm']	=	$data_send;
		if (!empty($_POST['judul'])) {
			$this->db->where('judul',$_POST['judul']);
		}
		
		if (!empty($_POST['deskripsi'])) {
			$this->db->where('deskripsi',$_POST['deskripsi']);
		}

		if (!empty($_POST['sampai_tanggal'])) {
			$this->db->where('sampai_tanggal',$_POST['sampai_tanggal']);
		}
		

		$list = $this->mod_datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" name="get-check" value="'.$field->id_special_event.'"></input>';
            $row[] = $field->judul;
            $row[] = $field->deskripsi;
            $row[] = $field->sampai_tanggal;
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	
	function filter()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/special_event/filter'],$data);
	}

}