<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class barang extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/


	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/barang/index','role/admin/page/barang/js_barang'],$data);
	}

	public function add_page()
	{
		$data['kategori_barang']	=	$this->my_where("kategori_barang",[])->result_array();
		$data['account']			=	$this->get_user_account();
		$this->my_view(['role/admin/page/barang/add_data','role/admin/page/barang/js_add_page'],$data);
	}

	function edit_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('barang',['id_barang'=>$value])->row_array();
				$data_edit[]	=	[
										'id_barang'							=>	$data_set['id_barang'],
										'nama_barang'						=>	$data_set['nama_barang'],
										'stok'								=>	$data_set['stok'],
										'harga_barang'						=>	$data_set['harga_barang'],
										'deskripsi'							=>	$data_set['deskripsi'],
										'foto'								=>	$data_set['foto'],
										'idkategori_fk'						=>	$data_set['idkategori_fk'],
				];
			}

			$data['kategori_barang']	=	$this->my_where("kategori_barang",[])->result_array();
			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/barang/edit_data','role/admin/page/barang/js_edit_page'],$data);
		} else {
			$this->get_data();
		}
		
	}

	function detail_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('barang',['id_barang'=>$value])->row_array();
				$data_edit[]	=	[
										'id_barang'							=>	$data_set['id_barang'],
										'nama_barang'						=>	$data_set['nama_barang'],
										'stok'								=>	$data_set['stok'],
										'harga_barang'						=>	$data_set['harga_barang'],
										'deskripsi'							=>	$data_set['deskripsi'],
										'diskon'							=>	$data_set['diskon'],
										'diskon_type'						=>	$data_set['diskon_type'],
										'foto'								=>	$data_set['foto'],
				];
			}

			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/barang/detail_page'],$data);
		} else {
			$this->get_data();
		}
		
	}


	function cetak_page()
	{
		$data['sum_selected']=0;
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('barang',['id_barang'=>$value])->row_array();
				$data_edit[]	=	[
										'id_barang'							=>	$data_set['id_barang'],
										'nama_barang'						=>	$data_set['nama_barang'],
										'stok'								=>	$data_set['stok'],
										'harga_barang'						=>	$data_set['harga_barang'],
				];
			}

			$data['data_edit']			=	$data_edit;
			$data['sum_selected']		=	count($_POST['send_data']);
			$data['input_selected']		=	implode(',', $_POST['send_data']);
			
		}

		$this->my_view(['role/admin/page/barang/cetak_page','role/admin/page/barang/js_cetak'],$data);
		
	}
	
	/*
		ADD DATA
	*/


	public function simpan_data()
	{

		$config2['upload_path']="./include/foto_produk";
        $config2['allowed_types']='gif|jpg|png';
        $config2['encrypt_name'] = TRUE;

        $this->load->library('upload',$config2);
        if($this->upload->do_upload("foto_produk")){
            $data2 = array('upload_data' => $this->upload->data());
            $data=[
			'nama_barang'		=>	$_POST['nama_barang'],
			'harga_barang'		=>	$_POST['harga_barang'],
			'stok'				=>	$_POST['stok'],
			'deskripsi'			=>	$_POST['deskripsi'],
			'idkategori_fk'		=>	$_POST['idkategori_fk'],
			'foto'				=>	$data2['upload_data']['file_name'],
			];

			
				if ($this->save_data('barang',$data)) {
					$this->get_data();
				}	else 	{
					echo "error";
				}
				
        }

		
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{

		$data=[];
		$data=[
				'nama_barang'		=>	$_POST['nama_barang'],
				'harga_barang'		=>	$_POST['harga_barang'],
				'stok'				=>	$_POST['stok'],
				'deskripsi'			=>	$_POST['deskripsi'],
				'idkategori_fk'		=>	$_POST['idkategori_fk'],
				];
		if (!empty($_FILES['foto_produk']['name'])) {
			
			$this->my_delete_file(FCPATH.'/include/foto_produk/'.$_POST['foto_lama']);
			$config2['upload_path']="./include/foto_produk";
	        $config2['allowed_types']='gif|jpg|png|jpeg';
	        $config2['encrypt_name'] = TRUE;

	        $this->load->library('upload',$config2);
	        if($this->upload->do_upload("foto_produk")){
	            $data2 = array('upload_data' => $this->upload->data());
	            $data['foto'] = $data2['upload_data']['file_name'];
				
	        }
        }

        

				
		if ($this->my_update('barang',$data,['id_barang'=>$_POST['id_barang']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}

	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete('barang',['id_barang'=>$value]);
		}
	}

	/*
		PRINT DATA
	*/

	function cetak_data()
	{
		$this->my_delete_file(FCPATH.'/include/pdf_temp');

		$data=[];
		$where_send		=	[];

		if ($_POST['data_yg_dicetak']	==	'manual') {
			
			if (!empty($_POST['nama_barang'])) {
				$where_send['nama_barang']	=	$_POST['nama_barang'];
			}

			if (!empty($_POST['is_active'])) {
				$where_send['is_active']	=	$_POST['is_active'];
			}

			
		} else if($_POST['data_yg_dicetak']	==	'pilih')
		{
			$data_selected = explode(',', $_POST['input_selected']);
			foreach ($data_selected as $key => $value) {
				$this->db->or_where('id_barang', $value);
			}
		}

		$data_set = $this->my_where('barang',$where_send);
		
		$url	=	($_POST['laporan']	==	'data')	?	'role/admin/page/barang/cetak_data'	:	'role/admin/page/barang/cetak_kartu';
		
	    if ($_POST['tipe_laporan'] == 'pdf') {

	    	$param	=	[
                'url'			=>	$url,
                'customPaper'	=>	array(0,0,381.89,595.28),
                'data_value'	=>	[
                	"data"=>$data_set->result_array()
                ],
                'name'			=>	md5(rand(0,9999999)),
                'pos' 			=> 'landscape'
            ];

            $this->my_pdf($param);

	    }
	    
	    else if($_POST['tipe_laporan'] == 'excel')

	    {
	    	
            $param  =   [
                'filename'			=>		'Jadwal Kegiatan Sekolah',
                'data_obj'			=>		$data_set->result(),
                'header_table'		=>		['Nama', 'tanggal mulai', 'tanggal selesai', 'keterangan'],
                'print_field'		=>		['nama_barang', 'tanggal_mulai', 'tanggal_selesai', 'keterangan']
            ];

            $this->my_export_excel($param);
        
	    }

	}



	/*
		MANIPULATE DATA
	*/

	public function datatable()
	{
		$data_send = [
			'table'				=>	'barang',
			'column_order'		=>	['nama_barang','harga_barang','stok','create_at'],
			'column_search'		=>	['nama_barang','harga_barang','stok','create_at'],
			'order'				=>	['id_barang'	=>	'DESC'],
		];

		$_POST['frm']	=	$data_send;
		if (!empty($_POST['nama_barang'])) {
			$this->db->where('nama_barang',$_POST['nama_barang']);
		}
		
		if (!empty($_POST['harga_barang'])) {
			$this->db->where('harga_barang',$_POST['harga_barang']);
		}

		if (!empty($_POST['stok'])) {
			$this->db->where('stok',$_POST['stok']);
		}
		

		$list = $this->mod_datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" class="checkbox" name="get-check" value="'.$field->id_barang.'"></input>';
            $row[] = $field->nama_barang;
            $row[] = $field->harga_barang;
            $row[] = $field->stok;
            $row[] = $field->deskripsi;
            $row[] = $field->create_at;
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}

	function get_produk()
	{

        $this->load->library('pagination');
		$halaman = $_POST['limit'];
		$page = $_POST['page'];

		$mulai = ($page>1) ? ($page * $halaman) - $halaman : 0;
        if ($_POST['nama_barang_search']!=="") {
        	$this->db->like("nama_barang", $_POST['nama_barang_search']);
        }
        if ($_POST['stok_search'] !== "" ) {
        	$this->db->where("stok ".$_POST['with_search'], $_POST['stok_search']);
        }
        if ($_POST['harga_search'] !== "" ) {
        	$this->db->where("harga_barang".$_POST['with_harga_search'], $_POST['harga_search']);
        }
        $query = $this->db->get('barang',$halaman,$mulai);
        $db=$query->result_array();
        $total=$query->num_rows();

        $pages = ceil($total/$halaman); 
        $data['pagination']='';

        $data['barang']		= $db;
        $data['page']		= $page;
		$this->my_view(['role/admin/page/barang/get_barang'],$data);
	}

	function get_pagination()
	{
		$halaman = $_POST['limit'];
		$page = $_POST['page'];
		$mulai = ($page>1) ? ($page * $halaman) - $halaman : 0;
        if ($_POST['nama_barang_search']!=="") {
        	$this->db->like("nama_barang", $_POST['nama_barang_search']);
        }
        if ($_POST['stok_search'] !== "" ) {
        	$this->db->where("stok ".$_POST['with_search'], $_POST['stok_search']);
        }
        if ($_POST['harga_search'] !== "" ) {
        	$this->db->where("harga_barang".$_POST['with_harga_search'], $_POST['harga_search']);
        }
        $total=$this->db->get('barang')->num_rows();

        $pages = ceil($total/$halaman); 
        $data['pagination']='';

        for ($i=1; $i <=$pages ; $i++) { 
            if ($i==($page)) {
                # code...
                $data['pagination'].='<li class="active"><a onclick="set_produk(halaman='.$i.',start='.$i.',end='.$pages.',limit=6);">'.$i.'</a></li>';
            }else{
                $data['pagination'].='<li><a onclick="set_produk(halaman='.$i.',start='.$i.',end='.$pages.',limit=6);">'.$i.'</a></li>';
            }
            
        }
        echo $data['pagination'];
	}
	
	function filter()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/barang/filter'],$data);
	}

}