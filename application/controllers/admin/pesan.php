<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class pesan extends MY_Controller {
	

	/*
		CHANGE PAGE
	*/


	public function get_data()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/pesan/index','role/admin/page/pesan/js_pesan'],$data);
	}

	public function add_page()
	{
		$data['account']	=	$this->get_user_account();
		$this->my_view(['role/admin/page/pesan/add_data','role/admin/page/pesan/js_add_page'],$data);
	}

	public function edit_page()
	{
		if (isset($_POST['send_data'])) {
			$data_edit=[];
			foreach ($_POST['send_data'] as $key => $value) {
				$data_set = $this->my_where('pesan',['id_pesan'=>$value])->row_array();
				$data_edit[]	=	[
										'id_pesan'							=>	$data_set['id_pesan'],
										'email'									=>	$data_set['email'],
										
				];
			}

			$data['data_edit']	=	$data_edit;
			$this->my_view(['role/admin/page/pesan/edit_data','role/admin/page/pesan/js_edit_page'],$data);
		} else {
			$this->get_data();
		}
		
	}

	/*
		ADD DATA
	*/


	public function simpan_data()
	{

		
            $data=[
			'email'					=>	$_POST['email'],
			];

			
				if ($this->save_data('pesan',$data)) {
					$this->get_data();
				}	else 	{
					echo "error";
				}
				
      

		
	}


	/*
		EDIT DATA
	*/

	function update_data()
	{
		
		$data=[];
		$data=[
				'email'		=>	$_POST['email'],
		];

		if ($this->my_update('pesan',$data,['id_pesan'=>$_POST['id_pesan']])) {
			$this->get_data();
		}	else 	{
			echo "error";
		}

	}

	/*
		DELETE DATA
	*/

	function hapus()
	{
		foreach ($_POST['data_get'] as $key => $value) {
			$this->db->delete('pesan',['id_pesan'=>$value]);
		}
	}

	
	/*
		MANIPULATE DATA
	*/

	public function datatable()
	{
		$data_send = [
			'table'				=>	'email_user',
			'column_order'		=>	['email','first_name','last_name','message','is_read','subject','create_at'],
			'column_search'		=>	['email','first_name','last_name','message','is_read','subject','create_at'],
			'order'				=>	['id_email_user'	=>	'DESC'],
		];

		$_POST['frm']	=	$data_send;
		if (!empty($_POST['email'])) {
			$this->db->where('email',$_POST['email']);
		}
		
		$list = $this->mod_datatable->get_datatables();
        $data = array();
        $no = $_POST['start'];
        foreach ($list as $field) {
            $no++;
            $row = array();
            $row[] = '<input type="checkbox" class="checkbox" name="get-check" value="'.$field->id_email_user.'"></input>';
            $row[] = $field->first_name.' '.$field->last_name.'('.$field->email.')'; 
            $row[] = substr($field->subject, 0,255);     
            $row[] = date_format(date_create($field->create_at),'d-m-Y');
            $data[] = $row;
        }
 
        $output = array(
            "draw" => $_POST['draw'],
            "recordsTotal" => $this->mod_datatable->count_all(),
            "recordsFiltered" => $this->mod_datatable->count_filtered(),
            "data" => $data,
        );
        //output dalam format JSON
        echo json_encode($output);
	}
	


}