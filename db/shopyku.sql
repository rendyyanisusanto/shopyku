/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 100138
 Source Host           : localhost:3306
 Source Schema         : shopyku

 Target Server Type    : MySQL
 Target Server Version : 100138
 File Encoding         : 65001

 Date: 31/07/2019 07:21:32
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for barang
-- ----------------------------
DROP TABLE IF EXISTS `barang`;
CREATE TABLE `barang`  (
  `id_barang` int(11) NOT NULL AUTO_INCREMENT,
  `nama_barang` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `harga_barang` double(20, 0) NULL DEFAULT NULL,
  `foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `stok` int(20) NULL DEFAULT NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  `diskon` double(20, 0) NOT NULL,
  `diskon_type` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT '0',
  PRIMARY KEY (`id_barang`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of barang
-- ----------------------------
INSERT INTO `barang` VALUES (5, 'Toshiba Tecra A40-D148', '<h2>Spesifikasi dari Laptop Toshiba Tecra A40-D148 - 14 inch - i5-7200U - 4GB DDR4 - 1TB - DVD-RW - WIN10 HOME - Garansi 3 thn</h2>\n\n<ul>\n	<li>Processor :Intel&reg; Core &trade; i5-7200U Processor 2.5GHz</li>\n	<li>Layar : 14.0&quot; HDAnti-Glare</li>\n	<li>Ram :4GB DDR4 2133 MHz</li>\n	<li>HDD :1TB 5400RPM</li>\n	<li>Graphics : Integrated Intel&reg; HD Graphics 620</li>\n	<li>Camera :HD Webcam withDual Microphone</li>\n</ul>\n', 10799000, 'd71443051ce2ee473549578bd1244581.jpg', 10, '2019-06-24 19:58:50', 90000, 'uang');
INSERT INTO `barang` VALUES (6, 'Dell Latitude E6230', '<p>Dell Latitude E6230&nbsp;<br />\nCore i5 gen 3<br />\nHdd 250-320gb<br />\nRam 4 gb<br />\nCamera dan no cam<br />\nVga intel hd 4000<br />\nBaterai 1-2 jam an<br />\nKondisi fisik campur, sktr 85-93%<br />\n<br />\nGaransi 1 minggu stlh barang diterima.<br />\nGaransi tdk berlaku apabila segel rusak, kesalahan pemakaian/human error.<br />\n<br />\nTerima kasih and happy shopping.</p>\n', 2500000, '5268c874a40e5d7f728eb822ba0fa12e.jpg', 100, '2019-06-24 20:00:08', 50, 'persen');
INSERT INTO `barang` VALUES (7, 'Lenovo Thinkpad X220', '<p>Laptop bisnis yg bandel,<br />\n<br />\nLenovo thinkpad X220<br />\nCore i7 2640M 2.8 ghz<br />\nHdd 500 gb<br />\nRam 4 gb<br />\nVga intel hd 3000<br />\nCamera<br />\nFisik mulus, sktr 88-92%.<br />\nBaterai 1-2 jam an.<br />\n<br />\nGaransi 1 minggu stlh barang diterima.<br />\nGaransi tdk berlaku apabila segel rusak, kesalahan pemakaian/human error.<br />\n<br />\nTerima kasih and happy shopping.</p>\n', 3195000, '92dbb45838cfec40a561e1a923383b09.jpg', 100, '2019-06-24 20:01:13', 0, NULL);
INSERT INTO `barang` VALUES (8, 'ASUS ROG STRIX ', '<p>ASUS ROG STRIX GL503GE-EN023T [i7-8750/8GB/1TB/Nvidia 1050Ti 4GB/W10]<br />\n<br />\n- Intel CoffeeLake 8th Core i7 8750H TURBO up to 4.1GHz (9MB Cache), 12 Threads<br />\n- 8GB RAM DDR4 2666MHz (Max. 32GB)<br />\n- 1TB 5400RPM + 8GB Hybrid HDD FireCuda (1x SSD NVME SLOT Available)<br />\n- 15.6&quot;FHD WVA Matte LED 120hz &#39;&#39;&#39;<br />\n- Intel UHD Grapchis 630&nbsp;<br />\n- nVidia GeForce GTX1050Ti 4GB DDR5<br />\n- SLiM DESiGN<br />\n- 2 x 3.5W speaker with Smart AMP technology<br />\n- RGB Backlit Keyboard Aurora<br />\n- HD Web Camera<br />\n- 1x Combo Audio Jack, 1x Multi Format Card Reader&nbsp;<br />\n- 3x USB 3.0 Port, 1x USB 2.0 Port, 1x USB 3.1 TYPE C Port<br />\n- 1x HDMI, 1x Mini Display Port, 1x RJ45 Gigabit LAN<br />\n- 802.11ac Dual Band WiFi AC9560 + Bluetooth v4.1<br />\n- 2.5kg included 4 cells battery 64WHrs<br />\n- Metal Black<br />\n- Windows 10 Home 64bit<br />\n<br />\nSpecial Features:<br />\n- New Design with Dual Fan Cooling System<br />\n- NVIDIA GeForce GTX 1050Ti 4GB (factory-overclocked by 100MHz)<br />\n- Marked WASD keys<br />\n- 1.8mm travel distance with 0.25mm keycap curve<br />\n- Hot keys: Volume up / Volume down / Mute / ROG Gaming Center<br />\n<br />\nGaransi Asus Indonesia 2 Tahun</p>\n', 15049000, 'e6968ff150a71ba3652c348da6f9d0b9.jpg', 100, '2019-06-24 20:02:55', 0, NULL);
INSERT INTO `barang` VALUES (9, 'LAPTOP HP 14-CM0066AU', '<p>LAPTOP HP 14-CM0066AU (BLACK) READY<br />\nLAPTOP HP 14-CM0067AU (SILVER) READY<br />\nLAPTOP HP 14-CM0068AU (GOLD) READY<br />\n<br />\nSPEC :<br />\n- AMD A9-9425 Dual-Core Processor (3.1 GHz base frequency, up to 3.7 GHz burst frequency, 1 MB cache)<br />\n- Windows 10 Home Single Language 64<br />\n- 4 GB DDR4-1866 SDRAM (1 x 4 GB)<br />\n- 1 TB 5400 rpm SATA<br />\n- AMD Radeon&trade; R5 Graphics<br />\n- Starting at 1.47 kg<br />\n- 14&quot; diagonal HD SVA BrightView WLED-backlit (1366 x 768)<br />\n<br />\nGARANSI RESMI HP INDONESIA 1 TAHUN</p>\n', 4249000, 'd7b0e562da20e1f75a485bfeb979f273.jpg', 10, '2019-06-24 20:03:50', 0, NULL);
INSERT INTO `barang` VALUES (10, 'ASUS A407MA ', '<p>A407MA-BV001T/02T<br />\n<br />\nCOLOR :<br />\n- BV001T : Grey<br />\n- BV002T : Gold<br />\n<br />\nSPESIFIKASI :<br />\n- Processor : Intel N4000<br />\n- Ram : 4 GB DDR4<br />\n- HDD : 1TB<br />\n- FINGERPRINT<br />\n- SLIM<br />\n- VGA : Integrated Intel HD Graphics<br />\n- Layar : 14&quot; 14.0&quot; (16:9) LED backlit HD (1366x768) Anti-Glare 60Hz Panel with 45% NTSC<br />\nSupport ASUS Splendid Technology<br />\n- Sistem Operasi : WINDOWS 10<br />\n- NONDVD<br />\n- CAM<br />\n- BLUETOOTH<br />\n- WIFI</p>\n', 3690000, '0ec24a23c4d4f1f01c207b645698bfc5.jpg', 100, '2019-06-24 20:04:54', 0, NULL);
INSERT INTO `barang` VALUES (11, 'asda', '<p>sad</p>\n', 12312, 'f079221752abc93c3b44861e0f9e4cf1.jpg', 1, '2019-07-08 21:18:53', 0, '0');

-- ----------------------------
-- Table structure for email_user
-- ----------------------------
DROP TABLE IF EXISTS `email_user`;
CREATE TABLE `email_user`  (
  `id_email_user` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `last_name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `subject` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `message` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `is_read` int(11) NULL DEFAULT NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_email_user`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of email_user
-- ----------------------------
INSERT INTO `email_user` VALUES (8, 'Rendy Yani', 'Susanto', 'gubuksoftware17@gmail.com', 'Backup data itlearningum.com', 'sadsa', 0, '2019-06-30 09:09:15');
INSERT INTO `email_user` VALUES (9, 'cicik', 'winarsih', 'cicikwinarsih@gmail.com', 'jkdas', 'ldkas;l', 0, '2019-06-30 09:11:29');
INSERT INTO `email_user` VALUES (10, 'Rendy Yani', 'winarsih', 'rendy@admin.com', 'Backup data itlearningum.com', 'l', 0, '2019-06-30 09:14:41');

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` mediumint(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES (1, 'admin', 'Administrator');
INSERT INTO `groups` VALUES (2, 'members', 'General User');

-- ----------------------------
-- Table structure for kegiatan
-- ----------------------------
DROP TABLE IF EXISTS `kegiatan`;
CREATE TABLE `kegiatan`  (
  `id_kegiatan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kegiatan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `foto_kecil` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `foto_besar` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `keterangan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_kegiatan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of kegiatan
-- ----------------------------
INSERT INTO `kegiatan` VALUES (1, 'Kegiatan Amal', 'item1.png', 'item1.png', 'Kegiatan bertajuk ibadah', '2019-05-05 15:00:55');
INSERT INTO `kegiatan` VALUES (2, 'Kegiatan Kerja Bakti', 'item2.png', 'item2.png', 'Kerja bakti bersama gubernur', '2019-05-05 15:01:36');
INSERT INTO `kegiatan` VALUES (3, 'Zakat', 'item3.png', 'item3.png', 'Zakat amal di masjid jami', '2019-05-05 15:02:04');
INSERT INTO `kegiatan` VALUES (4, 'Seminar Bisnis', 'item4.png', 'item4.png', 'Seminar bisnis international tahun 2019', '2019-05-05 15:02:38');
INSERT INTO `kegiatan` VALUES (5, 'Conference', 'item5.png', 'item5.png', 'Conference bersama para bisnisman', '2019-05-05 15:02:56');
INSERT INTO `kegiatan` VALUES (6, 'RUPS', 'item6.png', 'item6.png', 'Rapat Umum Pemegang Saham Tahun 2019', '2019-05-05 15:03:25');
INSERT INTO `kegiatan` VALUES (7, 'Rapat Bulanan', 'item7.png', 'item7.png', 'Rapat Bulanan tanggal 3 Maret 2019', '2019-05-05 15:03:55');
INSERT INTO `kegiatan` VALUES (8, 'Rapat Tahunan', 'item1.png', 'item8.png', 'Rapat Tahunan 2019', '2019-05-05 15:04:11');

-- ----------------------------
-- Table structure for keunggulan
-- ----------------------------
DROP TABLE IF EXISTS `keunggulan`;
CREATE TABLE `keunggulan`  (
  `id_keunggulan` int(11) NOT NULL AUTO_INCREMENT,
  `nama_keunggulan` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `text` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_keunggulan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of keunggulan
-- ----------------------------
INSERT INTO `keunggulan` VALUES (1, 'Produk Terjamin', 'Produk kami sangat terjamin kualitasnya sehingga user tidak perlu takut', '2019-05-05 14:07:29');
INSERT INTO `keunggulan` VALUES (2, 'Pelayanan Terbaik', 'Kami melayani pelanggan sepenuh hati', '2019-05-05 14:11:36');
INSERT INTO `keunggulan` VALUES (3, 'Partner yang berkelas', 'Partner kami sangat berkelas semua', '2019-05-05 14:14:02');

-- ----------------------------
-- Table structure for login_attempts
-- ----------------------------
DROP TABLE IF EXISTS `login_attempts`;
CREATE TABLE `login_attempts`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `login` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `time` int(11) UNSIGNED NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of login_attempts
-- ----------------------------
INSERT INTO `login_attempts` VALUES (1, '::1', 'rendy@admin.com', 1563542281);

-- ----------------------------
-- Table structure for misi
-- ----------------------------
DROP TABLE IF EXISTS `misi`;
CREATE TABLE `misi`  (
  `id_misi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_misi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_misi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of misi
-- ----------------------------
INSERT INTO `misi` VALUES (1, 'Menyediakan pupuk pupuk NPK padat dan pupuk dolomit bermutu', '2019-05-28 09:32:28');
INSERT INTO `misi` VALUES (2, 'Mengembangkan usaha melalui peningkatan hasil usaha', '2019-05-28 09:32:48');

-- ----------------------------
-- Table structure for pesan
-- ----------------------------
DROP TABLE IF EXISTS `pesan`;
CREATE TABLE `pesan`  (
  `id_pesan` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `last_name` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `subject` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `msg` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_pesan`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for profil_website
-- ----------------------------
DROP TABLE IF EXISTS `profil_website`;
CREATE TABLE `profil_website`  (
  `nama_website` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `alamat` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `no_hp` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `logo` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `icon` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `singkatan` varchar(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL DEFAULT NULL,
  `tagline` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `about_us` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `email` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `pemilik` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `facebook` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `twitter` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `youtube` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `instagram` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `img_about_us` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `wa_text` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `background` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of profil_website
-- ----------------------------
INSERT INTO `profil_website` VALUES ('Laptopku', 'Malang', '85894632505', 'bulb.png', 'bulb.png', 'LPT', 'Shop with the nature, Please shop with me', '<p>The Balance Small Business makes launching and managing your own business easy. It is home to experts who provide clear, practical advice on entrepreneurship and management. Whether you&rsquo;re just starting up or you want to take your company to the next level, our 20-year-strong library of more than 7,000 pieces of content will answer your questions and turn your business dreams into reality.</p>\n\n<p><a href=\"https://www.thebalancesmb.com/\">The Balance</a>&nbsp;is part of The Balance family of sites, including&nbsp;<a href=\"https://www.thebalance.com/\" rel=\"noopener\" target=\"_blank\">The Balance</a>&nbsp;and&nbsp;<a href=\"https://www.thebalancecareers.com/\" rel=\"noopener\" target=\"_blank\">The Balance Careers</a>, covering personal finance, career, and small business topics. With more than 24 million monthly visitors, The Balance is among the top-10 largest finance properties as measured by comScore, a leading Internet measurement company. Our more than 50 expert writers have extensive qualifications and expertise in their topics, including MBAs, PhDs, CFPs, other advanced degrees and professional certifications.</p>\n\n<p>The Balance family of sites have been honored by multiple awards in the last year, including&nbsp;<a href=\"https://www.tellyawards.com/winners/2017/short-form-social/general-education-discovery\" rel=\"noopener nofollow\" target=\"_blank\">The Telly Awards</a>,&nbsp;<a href=\"https://www.communicatorawards.com/\" rel=\"noopener nofollow\" target=\"_blank\">The Communicator Awards</a>, and&nbsp;<a href=\"https://www.editorandpublisher.com/news/editor-publisher-announces-the-2017-eppy-award-finalists/\" rel=\"noopener nofollow\" target=\"_blank\">Eppy Awards</a>.</p>\n', 'rendy@admin.com', 'Rendy Yani Susanto', 'https://www.facebook.com/rendy.y.susanto.31', 'https://www.twitter.com/rendy.y.susanto.31', 'https://www.youtube.com/rendy_yani_susanto/', 'https://www.instagram.com/rendy_yani_susanto/', 'a00ae551dd485419a57db462a6ae30b9.jpg', 'Halo Admin, Saya ingin bertanya/membeli/.....', 'd797c14a0070235734508b7d9f409395.jpg');

-- ----------------------------
-- Table structure for service
-- ----------------------------
DROP TABLE IF EXISTS `service`;
CREATE TABLE `service`  (
  `id_service` int(11) NOT NULL AUTO_INCREMENT,
  `judul` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `text` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `icon` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  PRIMARY KEY (`id_service`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of service
-- ----------------------------
INSERT INTO `service` VALUES (1, 'Computer Consulting', 'Kami menyediakan layanan konsultasi gratis', 'pie_chart');
INSERT INTO `service` VALUES (9, 'Servis Komputer', 'Kami menyediakan servis komputer dengan harga terjangkau', 'av_timer');
INSERT INTO `service` VALUES (10, 'Pembelian PC atau Notebook', 'Kami menyediakan PC atau notebook dengan kualitas terbaik', 'beenhere');

-- ----------------------------
-- Table structure for setting_website
-- ----------------------------
DROP TABLE IF EXISTS `setting_website`;
CREATE TABLE `setting_website`  (
  `themes` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of setting_website
-- ----------------------------
INSERT INTO `setting_website` VALUES ('resi');

-- ----------------------------
-- Table structure for special_event
-- ----------------------------
DROP TABLE IF EXISTS `special_event`;
CREATE TABLE `special_event`  (
  `id_special_event` int(11) NOT NULL AUTO_INCREMENT,
  `judul` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `deskripsi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `sampai_tanggal` date NULL DEFAULT NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_special_event`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of special_event
-- ----------------------------
INSERT INTO `special_event` VALUES (1, '1000 Diskon', 'Ayo buruan serbu, banyak diskon murah untuk event ini.', '2019-07-02', '2019-06-28 19:19:00');
INSERT INTO `special_event` VALUES (2, '100', 'sadk;a', '2019-06-10', '2019-06-28 19:22:45');
INSERT INTO `special_event` VALUES (4, 'Servis Komputer', '<p>dsads</p>\r\n', '2019-07-02', '2019-06-29 10:28:17');

-- ----------------------------
-- Table structure for subscriber
-- ----------------------------
DROP TABLE IF EXISTS `subscriber`;
CREATE TABLE `subscriber`  (
  `id_subscriber` int(11) NOT NULL AUTO_INCREMENT,
  `email` mediumtext CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_subscriber`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for testimoni
-- ----------------------------
DROP TABLE IF EXISTS `testimoni`;
CREATE TABLE `testimoni`  (
  `id_testimoni` int(11) NOT NULL AUTO_INCREMENT,
  `nama` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `foto` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `testimoni` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_testimoni`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of testimoni
-- ----------------------------
INSERT INTO `testimoni` VALUES (3, 'Yanuar Wahyu', 'bd8506f2c28bd726e87697a837ceb12e.jpg', '<p>Produk sangat bagus, garansi juga sangat keren.. semoga ditingkatkan lagi penanganan terhadap customer</p>\n', '2019-06-26 16:39:11');
INSERT INTO `testimoni` VALUES (4, 'Cicik Winarsih', 'dfe3a6b9743491f13cd78cadbc897b11.jpg', '<p>WOW sekali.. sangat cocok untuk bisnis rumahan loh</p>\n', '2019-06-26 16:39:48');
INSERT INTO `testimoni` VALUES (5, 'Achmad Syaifullah', 'bc1d3062c9f23781d2989f388f900b0e.jpg', '<p>Keren Heb.. mugo oleh bati akeh</p>\n', '2019-06-26 16:40:18');
INSERT INTO `testimoni` VALUES (6, 'Nirvan Saroful AmiN', 'c1e9fe1dac05e5f00c7d6df5c274a48f.jpg', '<p>Ayo dodolan bareng maneh coy</p>\n', '2019-06-26 16:40:43');

-- ----------------------------
-- Table structure for toko
-- ----------------------------
DROP TABLE IF EXISTS `toko`;
CREATE TABLE `toko`  (
  `id_toko` int(11) NOT NULL AUTO_INCREMENT,
  `nama_toko` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `url_toko` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `iduser_fk` int(11) UNSIGNED NULL DEFAULT NULL,
  `is_active` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id_toko`) USING BTREE,
  INDEX `iduser_fk`(`iduser_fk`) USING BTREE,
  CONSTRAINT `toko_ibfk_1` FOREIGN KEY (`iduser_fk`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `username` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `salt` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(254) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `activation_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `forgotten_password_time` int(11) UNSIGNED NULL DEFAULT NULL,
  `remember_code` varchar(40) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `created_on` int(11) UNSIGNED NOT NULL,
  `last_login` int(11) UNSIGNED NULL DEFAULT NULL,
  `active` tinyint(1) UNSIGNED NULL DEFAULT NULL,
  `first_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `company` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `phone` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `foto` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, '127.0.0.1', 'rendy yani susanto', '$2y$08$P/Vo2gHP8M4e6ClllGhSGOS3nfVTjoJ2tFaXukmqrWfRVDPAVKR6C', '', 'rendy@admin.com', '', NULL, NULL, NULL, 1268889823, 1563515817, 1, 'Rendy', 'Yani', 'ADMIN', '085894632505', '3c8f6f36f650d5ce07803470b4f4d4ff.jpg');

-- ----------------------------
-- Table structure for users_groups
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups`  (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` int(11) UNSIGNED NOT NULL,
  `group_id` mediumint(8) UNSIGNED NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `uc_users_groups`(`user_id`, `group_id`) USING BTREE,
  INDEX `fk_users_groups_users1_idx`(`user_id`) USING BTREE,
  INDEX `fk_users_groups_groups1_idx`(`group_id`) USING BTREE,
  CONSTRAINT `users_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `users_groups_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES (1, 1, 1);
INSERT INTO `users_groups` VALUES (2, 1, 2);

-- ----------------------------
-- Table structure for visi
-- ----------------------------
DROP TABLE IF EXISTS `visi`;
CREATE TABLE `visi`  (
  `id_visi` int(11) NOT NULL AUTO_INCREMENT,
  `nama_visi` text CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL,
  `create_at` timestamp(0) NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id_visi`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Compact;

-- ----------------------------
-- Records of visi
-- ----------------------------
INSERT INTO `visi` VALUES (1, 'Menjadi produsen yang memproduksikan pupuk NPK padat dan pupuk dolomit yang bermutu dan menjadi pilihan petani maji', '2019-05-28 09:25:17');

SET FOREIGN_KEY_CHECKS = 1;
