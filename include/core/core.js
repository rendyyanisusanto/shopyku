$(document).ready(function($) {
  
    if (window.location.hash.substr(1)!='') {
      set_content(window.location.hash.substr(1));
    }

    $(document.body).on('click', '.app-item' ,function(){
        set_content($(this).attr('href'));
        return false;
    });
    $(document.body).on('click', '#select_all' ,function(){
      if(this.checked){
        $('.checkbox').each(function(){
          this.checked = true;
        });
        }else{
        $('.checkbox').each(function(){
                    this.checked = false;
        });
        }
    });
        
    $(document.body).on('click', '.checkbox' ,function(){
        if($('.checkbox:checked').length == $('.checkbox').length){
          $('#select_all').prop('checked',true);
        }else{
          $('#select_all').prop('checked',false);
        }
    });
    
});

function set_content(url, data_send="")
{
  $('.se-pre-con').css('display','block');
  var url_set=url;
        
  $.ajax({
    url: url_set,
    datatype:'html',
    type:'POST',
    data:data_send, 
    success: function(result){
      window.location.hash = url_set;
      $(".se-pre-con").fadeOut("slow");
      $(".app-content").html(result);  
  }});
}

function send_ajax(url_get, data_get)
{
  return $.ajax({
    url : url_get,
    type: "POST",
    data: data_get,
    success: function () {
    },
    error: function (jXHR, textStatus, errorThrown) {
    }
    });
}

function send_ajax_file(url_get, data_get)
{
  return $.ajax({
    url : url_get,
    type: "POST",
    data: data_get,
    processData:false,
    contentType:false,
    cache:false,
    success: function () {
    },
    error: function (jXHR, textStatus, errorThrown) {
    }
    });
}




